\section{Classes}

\vspace{2em}

\begin{aconnaitre}
Lorsque l'on étudie un \textbf{caractère quantitatif} sur une série brute de données, pour \textbf{limiter la taille du tableau de données}, on est parfois amené \textbf{à regrouper les données par} \MotDefinition[classe]{classes}{} : on détermine alors les effectifs de chaque classe.
\end{aconnaitre}

\vspace{2em}

\begin{methode*1}[Regrouper des données par classes]
\exercice
On a demandé à 28 élèves leur taille en centimètres. La série brute constituée par les résultats de cette enquête est la suivante :

\renewcommand*\tabularxcolumn[1]{>{\centering\arraybackslash}m{#1}} 

\begin{tabularx}{\linewidth}{XXXXXXXXXXXXXX}
155 & 151 & 153 & 148 & 155 & 153 & 148 & 152 & 151 & 153 & 156 & 147 & 145 & 156 \\
154 & 156 & 149 & 153 & 155 & 152 & 149 & 148 & 152 & 156 & 153 & 148 & 148 & 150 \\
\end{tabularx}

\correction

La population étudiée est constituée par les élèves de la classe. Son effectif total est 28. Le caractère étudié --- leur taille --- est quantitatif.
Les tailles allant ici de 145\,cm à 156\,cm, on décide de regrouper ces données par classes d'amplitude 4\,cm.

\vspace{1em}

\renewcommand*\tabularxcolumn[1]{>{\centering\arraybackslash}m{#1}}
\begin{cltableau}{\linewidth}{4}
\hline
Taille comprise (en cm) & Entre 145 et 149 & Entre 150 et 154 & Entre 155 et 159 \\ \hline
Effectif & 9 & 12 & 7 \\ \hline
\end{cltableau}
\end{methode*1}


\vspace{2em}


\begin{exemple*1}
Une sage-femme a relevé le poids à la naissance des bébés qu’elle a aidés à mettre au monde au cours de son dernier mois de garde (poids en kilogrammes) :

\vspace{1em}

\renewcommand*\tabularxcolumn[1]{>{\centering\arraybackslash}m{#1}} 

\begin{tabularx}{\linewidth}{XXXXXXXXXXX}
3,97 & 4,27 & 2,89 & 3,09 & 4,17 & 2,31 & 2,57 & 3,44 & 4,13 & 2,27 & 3,5 \\
4,14 & 2,5 & 3,11 & 4 & 2,6 & 2,92 & 3,97 & 3,46 & 2,75 & & \\
\end{tabularx}

\vspace{1em}

Regroupe ces données par classes dans le tableau suivant.

\correction

\vspace{1em}

\renewcommand*\tabularxcolumn[1]{>{\centering\arraybackslash}m{#1}}
\begin{cltableau}{\linewidth}{6}
\hline
Poids $p$ (en kg) & $2 \leqslant p < 2,5$ & $... \leqslant p < ...$ & $... \leqslant p < ...$ &  $... \leqslant p < ...$ & $... \leqslant p  < ...$ \\ \hline
Effectif & & & & & \\ \hline
\end{cltableau}

\end{exemple*1}



\newpage


\section{Fréquence}

\vspace{2em}

\begin{aconnaitre}
La \MotDefinition{fréquence}{} \textbf{d'une valeur est le quotient :} $\dfrac{\text{effectif de la valeur}}{\text{effectif total}}$.

\vspace{.5em}

Elle peut être exprimée sous forme décimale (exacte ou approchée) ou fractionnaire.

\vspace{.5em}

Dans le cas de pourcentage, on parle de \textbf{fréquence en pourcentage}.
\end{aconnaitre}


\vspace{2em}


\begin{methode*1}[Calculer une fréquence]

\exercice

Dans une classe de 30 élèves, il y a 12 filles. Calcule la fréquence puis la fréquence en pourcentage des filles dans cette classe.

\correction

Il y a dans la classe 12 filles sur 30 élèves : la fréquence des filles est donc $\dfrac{12}{30}$.

Or $\dfrac{12}{30}=\dfrac{4 \times 3}{10 \times 3} = \dfrac{4}{10} = \dfrac{40}{100}$. Donc 40\,\%\ des élèves de cette classe sont des filles.

\end{methode*1}


\vspace{2em}

\begin{exemple*1}
À l’école maternelle Jean Moulin, il y a 120 enfants dont 36 en grande section, 54 en moyenne section et 30 en petite section. À l’école maternelle Alphonse Daudet, il y a 63 enfants en grande section, 72 en moyenne section et 45 en petite section. Calcule, pour chacune de ces deux écoles, la fréquence en pourcentage de chaque catégorie d'enfant.

\correction

\vspace{3cm}

\end{exemple*1}



\newpage

\section{Diagramme circulaire}


\vspace{2em}


\begin{aconnaitre}
L'angle de chaque secteur angulaire d'un diagramme circulaire (ou semi-circulaire) est \textbf{proportionnel} à l'effectif correspondant.

L'effectif total correspond à \textbf{un angle de 360°} (180° pour les semi-circulaires).

\textbf{On obtient l'angle en degrés en multipliant la fréquence par 360} (ou 180).
\end{aconnaitre}



\vspace{2em}



\begin{methode*1}[Construire un diagramme circulaire]

\exercice

Le recensement de l'INSEE de 1999 (sur la population française) montre que :
\begin{itemize}
    \item 14 951 165 personnes ont moins de 20 ans ;
    \item 32 555 443 ont entre 20 et 59 ans ;
    \item 12 680 597 ont plus de 60 ans.
\end{itemize}

\correction

On présente les calculs dans un tableau (valeurs arrondies au centième pour les fréquences et au degré pour les angles) :

\vspace{1em}

\renewcommand*\tabularxcolumn[1]{>{\centering\arraybackslash}m{#1}}
\begin{cltableau}{\linewidth}{5}
\hline
Tranche d’âge & Moins de 20 ans & Entre 20 et 59 ans & Plus de 60 ans & Total \\ \hline
Effectif & 14 951 165 & 32 555 443 & 12 680 597 & 60 187 205 \\ \hline
Fréquence & 0,25 & 0,54 & 0,21 & 1 \\ \hline
Angle (°) & 90 & 194 & 76 & 360 \\ \hline
\end{cltableau}

\vspace{1em}

Par exemple, pour les moins de 20 ans, la fréquence est :

$14 951 165 \div 60 187 205 \approx 0,25$ donc l'angle vaudra : $0,25 \times 360$° = 90°.

On construit ensuite le diagramme à l'aide d'un rapporteur.

\begin{center} \includegraphics[width=.5\linewidth]{statCours01}\end{center}

\end{methode*1}
 

\newpage

\begin{exemple*1}
À la fin de l’année scolaire 2002/03, l’orientation des élèves de 3\up{e} a donné les résultats suivants (source INSEE) :

\begin{itemize}
    \item 3e (Doublement) : 38 898
    \item 2nde : 362 573
    \item BEP : 151 736
    \item CAP: 36 626
    \item Autres : 456
\end{itemize}


Construis un diagramme semi-circulaire représentant ces données.

\correction

\vspace*{5cm}

\end{exemple*1}

