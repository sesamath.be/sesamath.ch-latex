


\section{Les isométries}

\hspace{2em}

\begin{aconnaitre}
Une \MotDefinition{isométrie}{} est une transformation qui conserve les longueurs. C'est donc une transformation par laquelle une figure et son image ont les mêmes dimensions : elles sont superposables.
\end{aconnaitre}

\vspace{2em}

\begin{exemple*1}   
Les 4 transformations étudiées en 6\up{e} sont des isométries : la symétrie centrale, la symétrie axiale, la translation et la rotation.

\begin{minipage}{.24\linewidth}
\centering
\includegraphics[width=.9\linewidth]{isoCours06}
\end{minipage}\hfill%
\begin{minipage}{.24\linewidth}
\centering
\includegraphics[width=.9\linewidth]{isoCours07}
\end{minipage}\hfill%
\begin{minipage}{.24\linewidth}
\centering
\includegraphics[width=.9\linewidth]{isoCours08}
\end{minipage}\hfill%
\begin{minipage}{.24\linewidth}
\centering
\includegraphics[width=.9\linewidth]{isoCours09}
\end{minipage}\\[1em]
\begin{minipage}[t]{.24\linewidth}
\centering
Symétrie centrale
\end{minipage}\hfill%
\begin{minipage}[t]{.24\linewidth}
\centering
Symétrie axiale
\end{minipage}\hfill%
\begin{minipage}[t]{.24\linewidth}
\centering
Translation
\end{minipage}\hfill%
\begin{minipage}[t]{.24\linewidth}
\centering
Rotation
\end{minipage}

\vspace{1em}

Sur chacun des exemples ci-dessus, la figure de départ et son image sont superposables.
\end{exemple*1}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage


\begin{aconnaitre}
Deux points $A$ et $A'$ sont symétriques par rapport au point $O$ lorsque le point $O$ est le milieu du segment $[AA']$.
\end{aconnaitre}


\vspace{2em}


\begin{methode*1}[Symétrique d'un point par une symétrie centrale]
Trace le point $A'$ symétrique du point $A$ par rapport au point $O$.

\begin{minipage}{.3\linewidth}
\begin{center}
    \includegraphics[width=.8\linewidth]{isoCours10}
\end{center}
\end{minipage}\hfill%
\begin{minipage}{.3\linewidth}
\begin{center}
    \includegraphics[width=.8\linewidth]{isoCours11}
\end{center}
\end{minipage}\hfill%
\begin{minipage}{.3\linewidth}
\begin{center}
    \includegraphics[width=.8\linewidth]{isoCours12}
\end{center}
\end{minipage}\\[1em]
\begin{minipage}[t]{.3\linewidth}
On trace la demi-droite $[AO)$.
\end{minipage}\hfill%
\begin{minipage}[t]{.3\linewidth}
On trace un arc de cercle de centre $O$ et de rayon $OA$. Il coupe la demi-droite $[AO)$ en un point.
\end{minipage}\hfill%
\begin{minipage}[t]{.3\linewidth}
On place le point $A'$ à l'intersection de la demi-droite $[AO)$ et de l'arc de cercle. On code la figure.
\end{minipage}

\exercice

\begin{enumerate}
    \item Trace un segment $[AB]$ de 5\,cm de longueur, puis construis le point $C$ symétrique de $B$ par rapport à $A$.
    \item Trace un segment $[RT]$ de 8,4\,cm de longueur, puis place le point $W$ tel que $R$ et $T$ soient symétriques par rapport au point $W$.
\end{enumerate}


\correction

\vspace*{4cm}

\end{methode*1}
     

%%%%%%%%%%%%%%%%%%%%%%%%%%
\newpage

\begin{aconnaitre}
Le symétrique d'un point $A$ par rapport à une droite $(d)$ est le point $M$ tel que la droite $(d)$ soit la médiatrice du segment $[AM]$ (tel que $(d)$ soit la perpendiculaire au segment $[AM]$ en son milieu).
\end{aconnaitre}

\vspace{1em}

\begin{remarque}
Si un point appartient à une droite alors son symétrique par rapport à cette droite est le point lui-même.
\end{remarque}

\vspace{2em}


\begin{methode*1}[Symétrique d'un point par une symétrie axiale]

Construis le point $S$, symétrique du point $P$ par rapport à la droite $(d)$.

\begin{minipage}{.3\linewidth}
\begin{center}
    \includegraphics[width=.8\linewidth]{isoCours13}
\end{center}
\end{minipage}\hfill%
\begin{minipage}{.3\linewidth}
\begin{center}
    \includegraphics[width=.8\linewidth]{isoCours14}
\end{center}
\end{minipage}\hfill%
\begin{minipage}{.3\linewidth}
\begin{center}
    \includegraphics[width=.8\linewidth]{isoCours15}
\end{center}
\end{minipage}\\[1em]
\begin{minipage}[t]{.3\linewidth}
On prend deux points distincts quelconques $M$ et $N$ sur la droite $(d)$.
\end{minipage}\hfill%
\begin{minipage}[t]{.3\linewidth}
On trace \textbf{deux arcs de cercle} de centres les deux points précédents et passant par $P$.
\end{minipage}\hfill%
\begin{minipage}[t]{.3\linewidth}
Ces deux arcs se coupent en un point qui est le point $S$.
\end{minipage}


\exercice


\begin{enumerate}
    \item Trace une droite $[AB]$ et un point $C$ puis construis le point $D$, symétrique de $C$ par rapport à la droite $[AB]$.

    \item Trace une droite $(d)$ et un segment $[RT]$ de 6\,cm de longueur puis construis le symétrique de $[RT]$ par rapport à la droite $(d)$.
    
    \item Trace un segment $[OU]$ de longueur 5,7\,cm. Le point $U$ est le symétrique du point $O$ par une symétrie axiale d'axe $(d)$. Construis la droite $(d)$.
\end{enumerate}


\correction

\vspace*{4cm}

\end{methode*1}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage


\begin{aconnaitre}
Une \MotDefinition{translation}{} consiste à faire glisser une figure selon un vecteur donné.
Un \MotDefinition{vecteur}{} est donné par :

\begin{minipage}{.66\linewidth}
\begin{itemize}
    \item une direction (c'est la direction de la droite)
    \item un sens, (c'est le sens de la flèche)
    \item une longueur (c'est la longueur du segment)
\end{itemize}
\end{minipage}\hfill%
\begin{minipage}{.31\linewidth}
\includegraphics[width=.8\linewidth]{isoCours16}
\end{minipage}

Un vecteur est représenté par une flèche.
\end{aconnaitre}

\vspace{2em}

\begin{methode*1}[Image d'un point par une translation]

Construis le point $A'$, image du point $A$ par la translation de vecteur $\vec{u}$.

\vspace{1em}

\begin{minipage}{.46\linewidth}
\centering
\includegraphics[width=.7\linewidth]{isoCours17}
\end{minipage}\hfill%
\begin{minipage}{.46\linewidth}
\centering
\includegraphics[width=.7\linewidth]{isoCours18}
\end{minipage}\\[1em]
\begin{minipage}[t]{.46\linewidth}
À partir du point $A$, on trace une flèche identique à la flèche qui représente le vecteur $\vec{u}$ : même direction, même sens et même longueur.
\end{minipage}\hfill%
\begin{minipage}[t]{.46\linewidth}
Le point $B$ est le point qui se trouve à l'extrémité de cette flèche.
\end{minipage}





\exercice

\begin{enumerate}
    \item Trace trois points $A$, $B$ et $C$. Construis l'image de $C$ par la translation de vecteur $\overrightarrow{AB}$ et l'image de $B$ par la translation de vecteur $\overrightarrow{CA}$.
    \item Trace un parallélogramme $ABCD$. Quelle est l'image de $D$ par la translation de vecteur $\overrightarrow{AB}$ ?
\end{enumerate}


\correction

\vspace*{4cm}

\end{methode*1}




\newpage

\begin{aconnaitre}
Une \MotDefinition{rotation}{} est définie par son \textbf{centre} et son \textbf{angle}.

L'angle de rotation est positif si la rotation s'effectue dans le sens contraire des aiguilles d'une montre et \textbf{négatif} sinon.
\end{aconnaitre}

\vspace{1em}

\begin{remarque}
La rotation de centre $O$ et d'angle $\alpha$ est notée : $R(O ; \alpha)$.
\end{remarque}


\vspace{2em}

\begin{methode*1}[Image d'un point par une rotation]

Construis le point $B$, image de $A$ par $R(O ; +60)$.

\vspace{1em}

\begin{minipage}{.3\linewidth}
\centering
\includegraphics[width=.9\linewidth]{isoCours19}
\end{minipage}\hfill%
\begin{minipage}{.3\linewidth}
\centering
\includegraphics[width=.9\linewidth]{isoCours20}
\end{minipage}\hfill%
\begin{minipage}{.3\linewidth}
\centering
\includegraphics[width=.9\linewidth]{isoCours21}
\end{minipage}\\[1em]
\begin{minipage}[t]{.3\linewidth}
On trace un arc de cercle de centre $O$ passant par le point $A$. On trace ensuite le segment $[OA]$.
\end{minipage}\hfill%
\begin{minipage}[t]{.3\linewidth}
Avec le rapporteur, on trace la demi-droite à $60^\circ$ du segment $[OA]$, dans le sens inverse des aiguille d'une montre.
\end{minipage}\hfill%
\begin{minipage}[t]{.3\linewidth}
Le point d'intersection de l'arc de cercle et de la demi-droite tracée est le point $B$.
\end{minipage}

\exercice

\begin{enumerate}
    \item Trace deux points $A$ et $B$. Construis l'image de $B$ par la rotation $R(A ; - 45)$ puis trace l'image de $A$ par la rotation $R(A ; + 30)$.
    \item Trace un triangle $ABC$ quelconque et construis l'image de ce triangle par la rotation $R(A ; -60)$.
\end{enumerate}


\correction

\vspace*{4cm}

\end{methode*1}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\newpage

\section{Enchaînement de plusieurs isométries}

\begin{aconnaitre}
On peut transformer une figure en enchaînant plusieurs isométries. Dans ce cas, la figure obtenue est encore superposable avec la figure de départ.
\end{aconnaitre}

\vspace{2em}

\begin{methode*1}[Enchaînement de plusieurs isométries]

On prend $A$, $B$, $C$, $D$ et $E$, cinq points dans le plan. On trace l'image du triangle $ABC$ par l'enchaînement de la symétrie centrale de centre $D$ et de la rotation $R(E ; +60)$.

\vspace{1em}

\begin{minipage}{.3\linewidth}
\includegraphics[width=.6\linewidth]{isoCours22}
\end{minipage}\hfill%
\begin{minipage}{.3\linewidth}
\centering
\includegraphics[width=.85\linewidth]{isoCours23}
\end{minipage}\hfill%
\begin{minipage}{.3\linewidth}
\centering
\includegraphics[width=.95\linewidth]{isoCours24}
\end{minipage}
\begin{minipage}[t]{.3\linewidth}
On commence par tracer l'image des points $A$, $B$ et $C$ par la symétrie centrale de centre $D$.
\end{minipage}\hfill%
\begin{minipage}[t]{.3\linewidth}
On obtient un triangle qu'on appelle $A'B'C'$. Puis on construis les images de $A'$, $B'$ et $C'$ par la rotation $R(E ; +60)$.
\end{minipage}\hfill%
\begin{minipage}[t]{.3\linewidth}
On obtient le triangle $A''B''C''$ qui est superposable au triangle $ABC$ de départ.
\end{minipage}

\exercice

\begin{enumerate}
    \item Trace un triangle $ABC$ quelconque. Construis l'image de ce triangle par l'enchaînement de la translation de vecteur $\overrightarrow{AB}$ et la rotation $R(B ; 45)$.
    \item Trace un carré $ABCD$ de côté 3\,cm. Construis l'image de ce carré par l'enchaînement de la symétrie axiale d'axe $(BC)$ et de la symétrie centrale de centre $A'$, où $A'$ est l'image de $A$ par la symétrie axiale d'axe $(BC)$.
\end{enumerate}

     
\correction

\vspace*{4cm}

\end{methode*1}

