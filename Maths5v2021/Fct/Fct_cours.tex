\section{Notion de fonction}

\begin{definition}
On considère deux ensembles de nombres : $A$ et $B$. Une fonction est une relation qui \textbf{à chaque élément de l'ensemble $A$} fait correspondre \textbf{un seul élément de l'ensemble $B$}.
\end{definition} 


\begin{minipage}{.6\linewidth}
\begin{remarque}
Si $f$ est le nom de la fonction, on note $f : A \rightarrow B$

$A$ est appelé « ensemble de départ » et $B$ « ensemble d'arrivée »
\end{remarque}
\end{minipage}\hfill%
\begin{minipage}{.38\linewidth}
\includegraphics[width=.7\linewidth]{fonCours01}
\end{minipage}





\begin{definition}
Si $a$ est un élément (un nombre) de $A$, on note $f(a)$ l'élément de $B$ qui correspond à $a$. On appelle cet élément l'\MotDefinition{image}{} \textbf{de $a$ par la fonction $f$} et on utilise aussi la notation  $f : a \xmapsto\ f(a)$ qui se lit « $f$ est la fonction qui, à l'élément $a$, associe le nombre $f(a)$ ».
\end{definition}


\begin{remarque}
Cette définition implique que chaque élément de A est en relation avec un et un seul élément de B. En revanche, certains éléments de B peuvent être en lien avec aucun ou plusieurs éléments de A.
\end{remarque}


\begin{exemple*1}
Pour trouver le périmètre d'un carré, on multiplie par 4 la longueur d'un côté :

\vspace{1em}

\renewcommand*\tabularxcolumn[1]{>{\centering\arraybackslash}m{#1}}
\begin{ctableau}{\linewidth}{6}
\hline
Longueur du côté & 1 & 1,5 & 12 & ... & $c$ \\ \hline
Périmètre & 4 & 6 & 48 & ... & $4\times c$ \\ \hline
\end{ctableau}

Avec cet exemple, on peut par exemple écrire : $f(12)=48$
\end{exemple*1}



\newpage


\section{Représentations d'une fonction}

On peut représenter une fonction de trois manières différentes : 
	
	\textbullet avec une expression fonctionnelle ;\\
	\textbullet avec un tableau de valeurs ;\\
	\textbullet avec une représentation graphique.

\begin{methode*1}[Représenter une fonction...]

\textbf{...avec une expression fonctionnelle}\\
On considère la fonction $f$ qui à un nombre associe ce nombre au carré moins 4. On appelle \MotDefinition{expression fonctionnelle} (ou expression algébrique) de $f$ l'écriture : $f:x \xmapsto\ x^2-4$ ou $f(x) = x^2 - 4$.

\vspace{1em}

\textbf{...avec un tableau de valeur}\\
Les images respectives par la fonction f de certaines valeurs de x peuvent être présentées dans un tableau appelé \MotDefinition{tableau de valeurs}{}.

\exercice



Voici un tableau de valeurs de la fonction $f : x \xmapsto\ x^2 - 4$

\vspace{.5em}

\renewcommand*\tabularxcolumn[1]{>{\centering\arraybackslash}m{#1}}
\begin{ctableau}{\linewidth}{10}
\hline
$x$ & $-4$ & $-3$ & $-2$ & $-1$ & 0 & 1 & 2 & 3 & 4 \\ \hline
$f(x)$ & 12 & 5 & 0 & $-3$ & $-4$ & $-3$ & 0 & 5 & 12 \\ \hline
\end{ctableau}

\vspace{.5em}

La 2\up{nde} ligne du tableau donne l'\textbf{image} de chaque nombre de la 1\up{ère} ligne par la fonction $f$.

\begin{enumerate}
\item On cherche 0 sur la 1\up{ère} ligne du tableau et on lit son \textbf{image} sur la 2\up{nde} ligne.

\textbf{L'image} de 0 par la fonction $f$ est $-4$. 

On écrit $f(0) = -4$ (ou $f : 0 \xmapsto\  -4$).

\item Dans l'autre sens, on peut chercher quels nombres de la 1\up{ère} ligne a donné $-3$ comme image : on cherche $-3$ sur la 2\up{ème} ligne du tableau et on trouve deux nombres sur la 1ère ligne : $-1$ et 1.
\end{enumerate}

\end{methode*1}


\newpage

\begin{methode*1}[Représenter une fonction avec une représentation graphique]
La \MotDefinition{représentation graphique d'une fonction}{} $f$ est la courbe constituée de l'ensemble des points de coordonnées $(x ; f (x))$.
 
\exercice

Tracer le graphique qui représente la fonction $f : x \xmapsto\  x^2 - 4$.

\correction

\begin{center}
\includegraphics[width=.5\linewidth]{fonCours02}
\end{center}

\end{methode*1}





\newpage


\section{Des fonctions particulières}


\begin{minipage}{.61\linewidth}
\begin{definition}
On appelle \MotDefinition{fonction constante} toute fonction qui, à tout nombre noté $x$, associe toujours le même nombre $b$. L'expression fonctionnelle d'une fonction constante est donc de la forme $f:x \xmapsto\ b$.
\end{definition} 

\vspace{1em}

\begin{propriete}
La représentation graphique d'une fonction constante est une \textbf{droite parallèle à l'axe des abscisses}.
\end{propriete} 
\end{minipage}\hfill%
\begin{minipage}{.38\linewidth}
\centering
\includegraphics[width=\linewidth]{fonCours03}

{\footnotesize Représentation de la fonction constante $f:x \xmapsto\ 2$}
\end{minipage}

\vspace{2em}

\begin{minipage}{.61\linewidth}
\begin{definition}
On appelle \MotDefinition{fonction linéaire}{} de coefficient $a$ toute fonction qui, à tout nombre noté $x$, associe le nombre $a \times x$ (c'est-à-dire $x \xmapsto\ a \times x$) où $a$ est un nombre.
\end{definition}

\begin{remarque}
Les fonctions linéaires traduisent des \textbf{situations de proportionnalité}.
\end{remarque}

\vspace{1em}

\begin{propriete}
La représentation graphique d'une fonction linéaire est une droite qui passe par l'origine du repère.
\end{propriete}

\end{minipage}\hfill%
\begin{minipage}{.38\linewidth}
\centering
\includegraphics[width=\linewidth]{fonCours04}

{\footnotesize Représentation graphique des fonctions linéaire $f:x \xmapsto\ x$, $g:x\xmapsto\ 0,5x$ et $h:x \xmapsto\ -2x$.}
\end{minipage}

\vspace{2em}

\begin{minipage}{.61\linewidth}
\begin{definition}
Si dans l'expression de la fonction $f$ on trouve des \textbf{$x$ au carré}, alors on dit que la fonction $f$ est une \MotDefinition{fonction quadratique}{}.
\end{definition} 

\begin{exemple*1}
Les fonctions suivantes sont des fonctions quadratiques :
\[ f : x \xmapsto\ x^2, g : x \xmapsto\ x^2 - 4 , h : x \xmapsto\ -x^2 + 1 \]
\end{exemple*1}

\vspace{1em}
\begin{propriete}
La représentation graphique d'une fonction quadratique a une forme appelée \textbf{parabole}.
\end{propriete} 
      
\end{minipage}\hfill%
\begin{minipage}{.38\linewidth}
\centering
\includegraphics[width=\linewidth]{fonCours05}

{\footnotesize Représentation graphique des fonctions quadratiques $f:x \xmapsto\ x^2$, $g:x \xmapsto\ x^2-4$ et $h:x \xmapsto\ -x^2 + 1$}
\end{minipage}


\newpage

\section{Représentations et lectures graphiques}

\begin{methode*1}[Représenter une fonction linéaire]

Pour tracer une droite, il suffit de connaître deux points qui appartiennent à cette droite. La représentation graphique d'une fonction linéaire est \textbf{une droite passant par l'origine du repère}. Pour la tracer, il suffit donc de connaître un point supplémentaire : on calcule pour cela \textbf{l'image d'un nombre par la fonction}.

\exercice
Représente graphiquement la fonction linéaire f définie par f(x) = - 0,5x.
\correction
$f$ est une fonction linéaire donc sa représentation graphique est une droite qui passe par l'origine du repère.

On choisit un nombre, par exemple 4 et on calcule son image par la fonction $f$ :

Comme $f(x) = - 0,5x$ ,  $f(4) = - 0,5 \times 4 = -2$

On trace la droite $(d_f)$ qui passe par l'origine et par le point $A(4 ; -2)$.

\begin{center}\includegraphics[width=.5\linewidth]{fonCours06}\end{center}
\end{methode*1}


\newpage

\begin{aconnaitre}[Lecture graphique]
Lorsqu'on cherche des valeurs à partir d'un graphique, on dit qu'on fait une \MotDefinition{lecture graphique}{}.
\end{aconnaitre}

\vspace{2em}


\begin{exemple*1}
Voici le graphique d'une fonction affine appelée $f$. Remplis le tableau de valeurs à partir d'une lecture graphique.

\renewcommand*\tabularxcolumn[1]{>{\centering\arraybackslash}m{#1}}
\begin{ctableau}{\linewidth}{5}
\hline
$x$ & $-3$ & 2 & 4 & \\ \hline
$f(x)$ & 3 & -2 & & 1 \\ \hline
\end{ctableau}

\begin{center}\includegraphics[width=.5\linewidth]{fonCours07}\end{center}

\correction
\begin{itemize}
\item Pour lire l'image de $-3$ :

On repère $-3$ \textbf{sur l'axe des abscisses}. On trace une droite verticale passant par l'abscisse $-3$. Elle coupe la droite $(d_f)$ en un point $A$. L'image de $-3$ est l'ordonnée du point $A$. Pour la lire, on trace une droite horizontale qui passe par $A$ et qui coupe l'axe des ordonnées. L'image de $-3$ par la fonction $f$ est 3.

\item Pour lire quel nombre a donné $-2$ par la fonction $f$ :

On repère $-2$ \textbf{sur l'axe des ordonnées}. On trace une droite horizontale passant par l'ordonnée $-2$. Elle coupe la droite $(d_f)$ en un point $B$. Le nombre cherché est l'abscisse du point $B$. Pour la lire, on trace une droite verticale qui passe par $B$ et qui coupe l'axe des abscisses. Le nombre cherché est 2.
\end{itemize}
\end{exemple*1}

\newpage


\begin{exemple*1}

Le graphique ci-dessous représente la fonction $f : x \xmapsto\ x^2 - 4$.
\begin{enumerate}
    \item Détermine graphiquement l'image de 1,5 par la fonction $f$.
    \item Détermine graphiquement les nombres qui ont $-3$ comme image par la fonction $f$.
\end{enumerate}

\begin{center}\includegraphics[width=.4\linewidth]{fonCours08} \hfill \includegraphics[width=.4\linewidth]{fonCours09}\end{center}

\correction

\begin{enumerate}        
\item On cherche l'ordonnée du point de la représentation graphique de $f$ qui a pour abscisse 1,5. Pour cela :
    \begin{itemize}
    \item On trace la droite parallèle à l'axe des ordonnées passant par le point d'abscisse 1,5.
    \item On trace la droite parallèle à l'axe des abscisses et qui passe par le point d'intersection de la représentation graphique de $f$ et de la droite précédente. Elle coupe l'axe des ordonnées en $-1,75$. 
    \end{itemize}
On en déduit que l'image de 1,5 par la fonction $f$ est $-1,75$ donc $f(1,5) = -1,75$.

\item On cherche l'abscisse (les abscisses) du (des) point(s) de la représentation graphique de $f$ ayant pour ordonnée $-3$. Pour cela :
    \begin{itemize}
    \item On trace la droite parallèle à l'axe des abscisses passant par le point d'ordonnée $-3$.
    \item On trace les droites parallèles à l'axe des ordonnées passant par les points d'intersection de la représentation graphique de $f$ et de la droite précédente. Ces parallèles coupent l'axe des abscisses en $-1$ et 1.
    \end{itemize}
      On en déduit que les deux nombres qui ont comme image $-3$ par la fonction f sont $-1$ et 1.
\end{enumerate}
\end{exemple*1}


