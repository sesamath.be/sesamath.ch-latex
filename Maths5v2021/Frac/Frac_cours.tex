
\section{Réduction de quotients au même dénominateur}

\begin{aconnaitre}
\begin{tabular}{p{.6\linewidth}|p{.35\linewidth}}
Pour additionner ou soustraire des fractions, il faut les mettre au \textbf{même dénominateur}. Pour cela, on utilise la règle suivante : \textbf{Si on multiplie ou si on divise} le numérateur et le dénominateur d'un quotient par \textbf{un même nombre non nul} alors on obtient \textbf{un quotient égal}. & Pour tous nombres $a$, $b$ et $k$ 
où $b$ et $k$ sont non nuls : \[ \dfrac{a\times k}{b \times k} = \dfrac{a}{b} \quad \text{et} \quad \dfrac{a \div k}{b \div k} = \frac{a}{b} \] \\
\end{tabular}
\end{aconnaitre}
 



\begin{exemple*1}

Réduis les quotients $\dfrac{2}{9}$ et $\dfrac{5}{12}$ au même dénominateur.

\correction

\begin{tabular}{lcl}
Multiple de 9 : 9, 18, 27, \textbf{36}, 45, 54,... & &\\
Multiple de 12 : 12, 24, \textbf{36}, 48, 60,... & $\longrightarrow$ & On cherche un multiple commun non nul aux \\
Un multiple commun de 9 et 12 est 36. & & dénominateurs (le plus petit possible). \\
C'est aussi le plus petit. &  &  \\
$\dfrac{2}{9} = \dfrac{2 \times \boldsymbol{4}}{9 \times \boldsymbol{4}} = \dfrac{8}{36}$ et $\dfrac{5}{12} = \dfrac{5\times \boldsymbol{3}}{12 \times \boldsymbol{3}} = \dfrac{15}{36}$ & $\longrightarrow$ & On détermine les écritures fractionnaires ayant \\
& &  36 pour dénominateur. \\
\end{tabular}
\end{exemple*1}


\begin{exemple*1}

Compare les quotients $\dfrac{2}{7}$ et $\dfrac{3}{8}$.

\correction

Les dénominateurs 7 et 8 n'ont aucun diviseur commun autre que 1.

Le plus petit multiple commun est $7 \times  8 = 56$, donc $\dfrac{2 \times \boldsymbol{8}}{7 \times \boldsymbol{8}} = \dfrac{16}{56}$ et $\dfrac{3 \times \boldsymbol{7}}{8 \times \boldsymbol{7}} = \dfrac{21}{56}$.

Rappel : deux fractions qui sont au même dénominateur sont rangées dans le même ordre que leur numérateur : Comme $16 < 21$ alors $\dfrac{16}{56}<\dfrac{21}{56}$ et finalement $\dfrac{2}{7}<\dfrac{3}{8}$.
\end{exemple*1}



\section{Addition ou soustraction}

\begin{aconnaitre}
\begin{tabular}{p{.6\linewidth}|p{.35\linewidth}}
Pour \textbf{additionner (ou soustraire)} des nombres en écriture fractionnaire \textbf{ayant le même dénominateur}, on additionne (ou on soustrait) les numérateurs et on garde le dénominateur commun. & Pour tous nombres $a$, $b$ et $c$ où $b$ est non nul : \[ \dfrac{a}{b}+\dfrac{c}{b}=\dfrac{a+c}{b} \] \\
\end{tabular}
\end{aconnaitre}



\begin{remarque}
Si les nombres en écriture fractionnaire n'ont pas le même dénominateur, il faut les réduire au même dénominateur.
\end{remarque}

\begin{exemple*1}

Calcule l'expression $A =-1+\dfrac{13}{30}-\dfrac{-11}{12}$.

\correction

\begin{tabular}{lcl}
Multiples de 30 : 30 ; \textbf{60} ; 90 ; 120... & & \\
Multiples de 12 : 12 ; 24 ; 36 ; 48 ; \textbf{60}... & $\longrightarrow$ & On cherche le plus petit multiple commun non nul à 30 et 12. \\
$A =\dfrac{-1 \times 60}{1 \times 60}+\dfrac{13 \times 2}{30 \times 2}+\dfrac{11 \times 5}{12 \times 5}$ & $\longrightarrow$ & On détermine le signe de chaque quotient et on réduit les \\
& & quotients au même dénominateur 60. \\
$A =\dfrac{-60}{60}+\dfrac{26}{60}+\dfrac{55}{60}=\dfrac{-60+26+55}{60}$ & $\longrightarrow$ & On additionne les numérateurs et on garde le dénominateur. \\
$A = \dfrac{21}{60}=\dfrac{7 \times 3}{20 \times 3}= \dfrac{7}{20}$ & $\longrightarrow$ & On simplifie si possible. \\
\end{tabular}

\end{exemple*1}



\section{Multiplication}

\begin{aconnaitre}
Pour \textbf{multiplier des nombres en écriture fractionnaire}, on multiplie les numérateurs entre eux et les dénominateurs entre eux.

Pour tous nombres $a$, $b$, $c$ et $d$ où $b$ et $d$ sont non nuls :
\[ \dfrac{a}{b} \times \dfrac{c}{d} = \dfrac{a\times c}{b \times d} \]
\end{aconnaitre}

\begin{remarque}
Si $b=1$, la formule devient $a \times \dfrac{c}{d} = \dfrac{a\times c}{d}$
\end{remarque}

\begin{exemple*1}
Calcule l'expression $B=-\dfrac{35}{33} \times \dfrac{-39}{-80}$. Donne le résultat sous forme simplifiée.
\correction
\vspace{.5em}
 
\begin{tabular}{lll}
$B = -\dfrac{35 \times 39}{33 \times 80}$ & $\longrightarrow$ & On détermine le signe du résultat. \\
$B = -\dfrac{7 \times \mathbf{5} \times 13 \times \mathbf{3}}{11 \times \mathbf{3} \times 2 \times \mathbf{5} \times 8}$ & $\longrightarrow$ & On cherche des facteurs communs. \\
$B = -\dfrac{7 \times 13}{11 \times 2 \times 8}$ & $\longrightarrow$ & On simplifie. \\
$B = -\dfrac{91}{176}$ & $\longrightarrow$ & On calcule. \\
\end{tabular}
\end{exemple*1}

\section{Division de deux quotients}

\subsection{Inversion d'un nombre non nul}

\begin{definition}
\textbf{Deux nombres sont \MotDefinition{inverses}{}} l'un de l'autre si leur produit est égal à 1.
\end{definition}

\begin{propriete}
\begin{itemize}
    \item Tout nombre $x$ non nul admet un inverse qui est le nombre $\dfrac{1}{x}$.
    \item Tout nombre en écriture fractionnaire $\dfrac{a}{b}$ ($a\neq 0$ et $b \neq 0$) admet un inverse qui est le nombre $\dfrac{b}{a}$.
\end{itemize}
\end{propriete}

\begin{remarque}
\begin{itemize}
    \item Un nombre et son inverse ont toujours le même signe.
    
    En effet, leur produit 1 est positif et seul le produit de deux nombres de même signe est positif.
    \item Zéro est le seul nombre qui n'admet pas d'inverse.
    
    En effet, tout nombre multiplié par 0 donne 0 et ne donnera jamais 1.
\end{itemize}
\end{remarque}

\begin{exemple}
Calcule l'inverse de 3.
\correction
L'inverse de 3 est $\dfrac{1}{3}$.
\end{exemple}

\begin{exemple}
Calcule l'inverse de $\dfrac{-7}{3}$.
\correction
L'inverse de $\dfrac{-7}{3}$ est $\dfrac{1}{\dfrac{-7}{3}}=\dfrac{3}{-7}=\dfrac{-3}{7}$.
\end{exemple}

\subsection{Diviser des quotients}

\begin{aconnaitre}
\textbf{Diviser par un nombre non nul} revient à multiplier par l'inverse de ce nombre.

Pour tous nombres $a$, $b$, $c$ et $d$ où $b$, $c$ et $d$ sont non nuls :
\[ \dfrac{a}{b} \div \dfrac{c}{d} = \dfrac{a}{b} \times \dfrac{d}{c} \text{ ou } \dfrac{\phantom{i}\dfrac{a}{b}\phantom{i}}{\dfrac{c}{d}}=\dfrac{a}{b} \times \dfrac{d}{c} \]
\end{aconnaitre}

\begin{exemple*1}
Calcule $C=\dfrac{-8}{7} \div \dfrac{5}{-3}$.
\correction
\vspace{.5em}
 
\begin{tabular}{lll}
$C=+\left(\dfrac{8}{7} \div \dfrac{5}{3}\right)$ & $\longrightarrow$ & On détermine le signe du résultat. \\
$C=\dfrac{8}{7} \times \dfrac{3}{5}$ & $\longrightarrow$ & On multiplie par l'inverse du deuxième quotient. \\
$C=\dfrac{8\times 3}{7\times 5}$ & $\longrightarrow$ & On multiplie les fractions. \\
$C=\dfrac{24}{35}$ & $\longrightarrow$ & On calcule. \\
\end{tabular}
\end{exemple*1}


\begin{exemple*1}
Calcule $D=\dfrac{-\dfrac{32}{21}}{\dfrac{-48}{-35}}$ et donne le résultat en le simplifiant le plus possible.
\correction
\vspace{.5em}
 
\begin{tabular}{lll}
$D=-\dfrac{\dfrac{32}{21}}{\dfrac{48}{35}}$ & $\longrightarrow$ & On détermine le signe du résultat. \\
$D=-\dfrac{32}{21}\times \dfrac{35}{48}$ & $\longrightarrow$ & On multiplie par l'inverse du deuxième quotient. \\
$D=-\dfrac{\mathbf{8}\times \mathbf{2}\times 2 \times \mathbf{7} \times 5}{\mathbf{7 \times 3 \times 3 \times \mathbf{2} \times \mathbf{8}}}$ & $\longrightarrow$ & On cherche des facteurs communs. \\
$D=-\dfrac{10}{9}$ & $\longrightarrow$ & On calcule sans oublier de simplifier avant ! \\
\end{tabular}
\end{exemple*1}


\begin{exemple*1}
Quelle est la nature du nombre $E$ défini par $E=\dfrac{1+\dfrac{2}{3}}{1-\dfrac{2}{3}}$ ?
\correction
\vspace{.5em}
 
\begin{tabular}{lll}
$E=\dfrac{\dfrac{3}{3}+\dfrac{2}{3}}{\dfrac{3}{3}-\dfrac{2}{3}}=\dfrac{\dfrac{5}{3}}{\dfrac{1}{3}}$ & $\longrightarrow$ & $E$ peut s'écrire aussi $E=\left(1+\dfrac{2}{3}\right) \div \left(1-\dfrac{2}{3}\right)$. \\
 & & On commence donc par calculer les parenthèses.\\
$E=\dfrac{5}{3} \times \dfrac{3}{1}$ & $\longrightarrow$ & On multiplie par l'inverse du deuxième quotient. \\
$E=\dfrac{5\times \mathbf{3}}{\mathbf{3}\times 1}$ & $\longrightarrow$ & On cherche des facteurs communs. \\
$E=5$ donc $E$ est un nombre entier. & &  \\
\end{tabular}
\end{exemple*1}


\section{Signe d'une fraction}

\begin{propriete}
Pour déterminer le signe d'une fraction, on utilise la règle du produit des signes.
\end{propriete}

\begin{aconnaitre}
Si une fraction est négative, on peut l'écrire de trois manières différentes en mettant le signe moins au numérateur, au dénominateur (peu utilisé) ou devant la fraction.
On écrira donc indifféremment  $\dfrac{-2}{3}$ ou $-\dfrac{2}{3}$ et rarement $\dfrac{2}{-3}$.
\end{aconnaitre}


