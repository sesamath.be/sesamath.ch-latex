\section{Prismes droits et cylindres : définition}

\subsection{Prisme droit}

\begin{definition}
Un \MotDefinition{prisme droit}{} est un solide qui a \textbf{deux faces parallèles et superposables} qui sont des polygones : on les appelle \textbf{les bases}.

Les autres faces du prisme droit sont \textbf{des rectangles} : on les appelle les \textbf{faces latérales}.
\end{definition} 

\begin{exemple*1}
Prismes droits représentés en perspective cavalière

\begin{center}
    \includegraphics[width=.8\linewidth]{prismCours01}
\end{center}
\end{exemple*1}

 

\subsection{Cylindre}

\begin{definition}
Un \MotDefinition{cylindre de révolution} est un solide que l'on obtient en faisant tourner un rectangle autour de l'un de ses côtés.

Un cylindre de révolution a \textbf{deux faces parallèles et superposables} qui sont des \textbf{disques} : ce sont \textbf{les bases} du cylindre.

La \MotDefinition{hauteur d'un cylindre}{} de révolution est la longueur du segment qui joint les centres des bases.
\end{definition}


\begin{exemple*1}
les figures ci-dessous représentent le même cylindre dans deux perspectives cavalières différentes.

\begin{center}
    \includegraphics[width=.8\linewidth]{prismCours02}
\end{center}
\end{exemple*1}


\section{Patron d'un prisme droit ou d'un cylindre}

\begin{exemple*1}
Dessine le patron d'un prisme droit dont la base est un triangle de côtés 5\,cm, 4\,cm et 3\,cm, et dont la hauteur est égale à 2\,cm.
\correction

\begin{minipage}{.29\linewidth}
\centering
\includegraphics[width=.5\linewidth]{prismCours03}
\end{minipage}\hfill%
\begin{minipage}{.29\linewidth}
\centering
\includegraphics[width=.8\linewidth]{prismCours04}
\end{minipage}\hfill%
\begin{minipage}{.29\linewidth}
\centering
\includegraphics[width=.8\linewidth]{prismCours05}
\end{minipage}

\begin{minipage}[t]{.29\linewidth}
On construit une des bases, qui est un triangle, puis on trace une face latérale qui est un rectangle dont les côtés sont un côté de la base et la hauteur du prisme droit.
\end{minipage}\hfill%
\begin{minipage}[t]{.29\linewidth}
On trace la seconde base, qui est un triangle symétrique au premier par rapport à l'un des axes de symétrie du rectangle.
\end{minipage}\hfill%
\begin{minipage}[t]{.29\linewidth}
On complète le patron en traçant les deux dernières faces latérales du prisme droit, qui sont des rectangles.
\end{minipage}
\end{exemple*1}






\begin{exemple*1}
Dessine le patron d'un cylindre de révolution de hauteur 3\,cm ayant pour base un disque de rayon 1\,cm.
\correction

\begin{minipage}[c]{.29\linewidth}
\centering
\includegraphics[width=.3\linewidth]{prismCours06}
\end{minipage}\hfill%
\begin{minipage}[c]{.29\linewidth}
\centering
\includegraphics[width=.8\linewidth]{prismCours07}
\end{minipage}\hfill%
\begin{minipage}[c]{.29\linewidth}
\centering
\includegraphics[width=.8\linewidth]{prismCours08}
\end{minipage}

\begin{minipage}[t]{.29\linewidth}
On construit une des bases du cylindre, qui est un disque de rayon 1\,cm. 

Le périmètre de ce cercle est $2 \times \pi \times 1$\,cm soit environ 6,28\,cm.
\end{minipage}\hfill%
\begin{minipage}[t]{.29\linewidth}
On trace la surface latérale du cylindre, qui est un rectangle dont les côtés sont la hauteur du cylindre et le périmètre du cercle, qui est environ 6,28\,cm. Le cercle et le rectangle se touchent en un point.
\end{minipage}\hfill%
\begin{minipage}[t]{.29\linewidth}
On complète le patron en traçant la seconde base, qui est un disque superposable au premier.
\end{minipage}
\end{exemple*1}



\begin{exemple*1}
\begin{enumerate}
    \item Dessine un patron d'un prisme droit de hauteur 3\,cm ayant pour base un triangle $ABC$ rectangle en $A$ tel que $AB = 2,5$\,cm et $AC = 4$\,cm.
    \item Dessine un patron d'un cylindre de révolution de rayon de base 2,5\,cm et de hauteur 7\,cm.
\end{enumerate}
\correction

\vspace{4cm}

\end{exemple*1}



\vspace{2em}

\section{Aire latérale d'un prisme droit ou d'un cylindre}

\vspace{1em}

\begin{aconnaitre}
Pour calculer l'\textbf{aire latérale d'un prisme droit ou d'un cylindre}, on multiplie le périmètre d'une base par la hauteur :  $\mathcal{A}_\text{latérale} = \mathcal{P}_\text{base} \cdot h$

Pour calculer l'aire totale d'un cylindre, on ajoute à l'aire latérale l'aire des deux disques de base.
\end{aconnaitre}

\vspace{2em}

\begin{exemple*1}
Détermine l’aire latérale d'un prisme droit de hauteur 10\,cm ayant pour base un parallélogramme $ABCD$ tel que $AB = 5$\,cm et $BC = 3$\,cm.
\correction
On calcule le périmètre du parallélogramme $ABCD$ qui est une base du prisme droit :
\[ \mathcal{P}_\text{base} = 2 \cdot (AB + BC) = 2 \cdot (5 + 3) = 2 \cdot 8 = 16 \text{ cm}. \]
On multiplie le périmètre d'une base par la hauteur :
\[ \mathcal{A}_\text{latérale} = \mathcal{P}_\text{base} \cdot h = 16 \cdot 10 = 160 \text{ cm}^2. \]
L'aire latérale de ce prisme droit vaut 160\,cm\up{2}.
\end{exemple*1}


\newpage


\begin{exemple*1}
Détermine l'aire latérale puis l'aire totale du cylindre de révolution suivant :
\begin{center}
    \includegraphics[width=.28\linewidth]{prismCours09}
\end{center}

\correction
On calcule le périmètre d'une base qui est un disque de rayon 4 cm :
\[ \mathcal{P}_\text{base} = 2 \times \pi \times 4 = 8 \pi \text{ cm} \]

On multiplie le périmètre d'une base par la hauteur :
\[\mathcal{A}_\text{latérale} = \mathcal{P}_\text{base} \times h = 8 \pi \times 7 = 56 \pi \text{ cm}^2 \]

L'aire latérale de ce cylindre de révolution vaut $56\pi$ cm$^2$. 

Avec 3,14 comme valeur pour $\pi$, cela donne 175,84\,cm$^2$.

L'aire de chacun des disques de base est : 
\[ \mathcal{A}_\text{base} = \pi \times 4^2 = 16 \pi \text{ cm}^2 \]

L'aire totale de ce cylindre de révolution vaut :
\[ \mathcal{A}_\text{totale} = 56 \pi + 2 \times 16 \pi = 88 \pi \text{ cm}^2 \]

Avec 3,14 comme valeur pour $\pi$, cela donne 276,32\,cm$^2$.
\end{exemple*1}



\vspace{2em}

\begin{exemple*1}
\begin{enumerate}
    \item Calcule l'aire latérale d'un prisme droit de hauteur 9\,cm ayant pour base un carré de côté 3\,cm.
    \item Calcule l'aire latérale puis l'aire totale d'un cylindre de hauteur 1,5\,dm et dont la base a un rayon de 5\,cm.
\end{enumerate}
\correction

\vspace{4cm}

\end{exemple*1}

\newpage


\section{Volume d'un prisme droit ou d'un cylindre}

\begin{aconnaitre}
Pour calculer \MotDefinition{le volume d'un prisme droit ou d'un cylindre}{}, on multiplie l’aire d’une base par la hauteur :  $V = \mathcal{A}_\text{base} \cdot h$.
\end{aconnaitre}

\begin{exemple*1}
Détermine le volume du prisme droit suivant :

\begin{center}
    \includegraphics[width=.25\linewidth]{prismCours10}
\end{center}

\correction
On calcule l'aire d'une base qui est un triangle rectangle :
\[ \mathcal{A}_\text{base} = \dfrac{4 \cdot 3}{2} = \dfrac{12}{2} = 6 \text{ cm}^2 \]

On multiplie l'aire d'une base par la hauteur :
\[ V = \mathcal{A}_\text{base} \cdot h = 6 \cdot 5 = 30 \text{ cm}^3.\]

Le volume de ce prisme droit vaut 30\,cm$^3$.
\end{exemple*1}




\begin{exemple*1}
Détermine le volume d'un cylindre de hauteur 4 cm ayant pour base un disque de rayon 3 cm :

\begin{center}
    \includegraphics[width=.2\linewidth]{prismCours11}
\end{center}
\correction
On calcule l'aire d'une base qui est un disque de rayon 3\,cm.
\[ \mathcal{A}_\text{base} = \pi \cdot 3^2 = 9 \text{ cm}^2.\]

On multiplie l'aire d'une base par la hauteur :
\[ V = \mathcal{A}_\text{base} \cdot h = 9 \pi \cdot 4 = 36 \pi \text{ cm}^3 \]

Avec 3,14 comme valeur pour $\pi$, on obtient : $V = 113,04$\,cm$^3$. 
\end{exemple*1}


\begin{exemple*1}
\begin{enumerate}
    \item Calcule le volume d'un prisme droit de hauteur 8\,cm ayant pour base un rectangle de longueur 5\,cm et de largeur 3\,cm.
    \item Calcule le volume d'un cylindre de révolution de hauteur 4,5\,cm ayant pour base un disque de diamètre 10\,cm. 
\end{enumerate}
\correction

\vspace{4cm}

\end{exemple*1}

