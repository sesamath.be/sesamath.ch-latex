\section{Factorisation}

\begin{definition}
\MotDefinition{Factoriser}{}, \textbf{c’est transformer une somme de termes en un produit de facteurs}.

La factorisation est l'inverse du développement (distributivité). Elle a pour but de transformer une somme en un produit de facteurs.
\end{definition}


\begin{propriete}
Pour tous nombres relatifs $k$, $a$ et $b$ : 

\[ \boldsymbol{k} \cdot a + \boldsymbol{k} \cdot b = \boldsymbol{k} \cdot (a + b) \]

\[ \boldsymbol{k} \cdot a - \boldsymbol{k} \cdot b = \boldsymbol{k} \cdot (a - b) \]
\end{propriete}
 


\section{Factoriser un nombre, une lettre ou une expression}

\vspace{3em}

\textbf{Cas où le facteur commun est un nombre :} factoriser $A = 14\,a - 7\,b$

\begin{tabular}{lcl}
$A = 7 \cdot 2\,a - 7 \cdot b$ & $\longrightarrow$ & On met en évidence le facteur commun : 7 \\
$A = 7(2\,a - b)$ & $\longrightarrow$ & On met en facteur le nombre 7 puis on regroupe les facteurs restants. \\
\end{tabular}

\vspace{2em}

\begin{exemple*1}
Factoriser les expressions $15x + 6y$ et $8x - 12y$
\correction
\vspace{2cm}
\end{exemple*1}


\vspace{3em}

\textbf{Cas où le facteur commun est une lettre :} factoriser $B = -x^2 + 3x$

\begin{tabular}{lcl}
$B = (-x) \cdot x + 3 \cdot x$ & $\longrightarrow$ & On replace les signes \og $\cdot$ \fg\ sous-entendus dans l'expression et on repère \\
& &  le facteur commun : $x$ \\
$B = x(-x + 3)$ & $\longrightarrow$ & On met en facteur $x$ puis on regroupe les facteurs restants. \\
\end{tabular}


\begin{exemple*1}
Factoriser les expressions $3x^2 + 23xy$ et $-5t^3 + 7t$ 
\correction
\vspace{2cm}
\end{exemple*1}


\newpage

\textbf{Cas où le facteur commun est une expression entre parenthèses :} $C = 4(x + 5)+ 3(x + 5)$

\begin{tabular}{lcl}
$C = 4\cdot(x + 5) + 3(x + 5)$& $\longrightarrow$ & On met en évidence le facteur commun : $x + 5$ \\
$C = (x + 5)(4 + 3)$& $\longrightarrow$ & On met en facteur $x + 5$ puis on regroupe les facteurs restants. \\
$C = 7(x + 5)$&  &  \\
\end{tabular}


\begin{exemple*1}
Factoriser les expressions $-5(x-3) + (x-3)(y-2)$ et $(y+6)(x-2)-4x(y+6)$
\correction
\vspace{2cm}

\end{exemple*1}

 

\vspace{3em}

\textbf{Cas avec des parenthèses :}   $E = (9x-4)(5x+6) - (9x-4)(3x+11)$

\begin{tabular}{lcl}
$I = (9x - 4)(5x + 6) - (9x - 4)(3x + 11)$ & $\longrightarrow$ & On repère un facteur commun. \\
$I = (9x - 4)[(5x + 6) - (3x + 11)]$ & $\longrightarrow$ & On factorise en utilisant des crochets. \\
$I = (9x - 4)[5x + 6 - 3x - 11]$ & $\longrightarrow$ & On supprime les parenthèses à l'intérieur des crochets en\\
& & faisant attention au signe « $-$ ». \\
$I = (9x - 4)(2x - 5)$ & $\longrightarrow$ & On réduit l'expression à l'intérieur des crochets. \\
\end{tabular}


\begin{exemple*1}
Factoriser l'expression $(7y - 2)(5x - 1) - (2x + 1)(7y - 2)$
\correction
\vspace{2cm}

\end{exemple*1}



\section{Lorsqu'il y a plusieurs facteurs communs}

\vspace{2em}
\textbf{Dans certaines expressions, il peut y avoir plusieurs facteurs communs.}

Par exemple $6x^2 - 15x$ se décompose en $3 \times x \times 2 \times x - 3 \times x \times 5$ : il y a donc deux facteurs communs : 3 et $x$.

On peut donc factoriser 3 pour obtenir : $6x^2 - 15x = 3$ ($x \times 2 \times x - x \times 5) = 3 (2x^2 - 5x)$

On peut aussi factoriser $x$ pour obtenir : $6x^2 - 15x = x ( 3 \times 2 \times x - 3 \times 5) = x (6x - 15)$

Enfin, on peut factoriser 3 et $x$  pour obtenir :  $6x^2 - 15x = 3x ( 2 \times x - 5) = 3x (2x - 5)$.

\vspace{2em}

\begin{aconnaitre}
D'une manière générale, lorsqu'on demande de factoriser une expression, il faut factoriser \textbf{tous les facteurs communs}. 

Ainsi, dans l'exemple ci-dessus, si on demande de factoriser $6x^2 - 15x$ sans plus de précision, on donnera le résultat sous la forme $3x (2x - 5)$.
\end{aconnaitre}


\newpage

\textbf{Cas où il y a plusieurs facteurs communs :} $D = 6x^2 - 15x$

\begin{tabular}{lcl}
$D = 3 \cdot 2 \cdot x \cdot x - 3 \cdot 5 \cdot x$ & $\longrightarrow$ & On décompose au maximum l'expression \\
$D = 3x \cdot 2x - 3x \cdot 5$& $\longrightarrow$ & On met en évidence les facteurs communs : 3 et $x$ \\
$D = 3x(2x - 5)$ & $\longrightarrow$ & On met en facteur $3x$ puis on regroupe les facteurs restants.\\
\end{tabular}

\vspace{2em}

\begin{exemple*1}
Factoriser les expressions $14x + 21x^3$ et $10xy^2 - 6x^2y$.
\correction
\vspace{2cm}

\end{exemple*1}

\vspace{3em}

\section{Cas où un terme complet est le facteur commun}



\begin{aconnaitre}
Lorsqu'on factorise un terme complet, il faut bien penser qu'il est facteur de 1 et non de zéro :
\[ k \cdot a + k =k \cdot a + k \boldsymbol{\times 1} = k \cdot (a + \boldsymbol{1}) \]
\end{aconnaitre}

\vspace{3em}

\textbf{Cas du terme complet qui est facteur commun :} $E = 3xy - 3x$

\begin{tabular}{lcl}
$E = 3x \cdot y - 3x$ & $\longrightarrow$ & On repère le facteur commun. \\
$E = 3x \cdot y - 3x \times 1$ & $\longrightarrow$ & Le deuxième terme au complet est facteur commun : on pense qu'il est facteur de 1. \\
$E = 3x ( y - 1)$& $\longrightarrow$ & On factorise $3x$ sans oublier le 1 dans la parenthèse. \\
\end{tabular}

\vspace{2em}

\begin{remarque}
\textbf{Attention !} Ce cas particulier est source de nombreuses erreurs, en particulier lorsqu'on doit factoriser des expressions du type : $(x - 4) - (x - 4)(5x + 2)$ pour lesquelles il faut bien appliquer la règle et faire attention au signe $-$ :

\begin{align*}
    (x - 4) - (x - 4)(5x + 2) 	&= (x - 4)\boldsymbol{\times 1} - (x - 4 )(5x + 2) \\
	&= (x - 4)[\boldsymbol{1} - (5x + 2)] = (x - 4)(\boldsymbol{1} - 5x - 2)\\
	&= (x - 4)(- 5x - 1)\\
\end{align*}
\end{remarque}

\vspace{2em}

\begin{exemple*1}
Factoriser les expressions $7x(x + 2) + 7x$ et $(5y + 3) - (5y + 3)(1 - y)$
\correction
\vspace{2cm}

\end{exemple*1}

