\section{Grandeurs proportionnelles}

\vspace{2em}

\subsection{Quatrième proportionnelle}

\vspace{1em}

\begin{propriete}
\begin{minipage}{.75\linewidth}
Dans une situation de proportionnalité, la \MotDefinition{quatrième proportionnelle}{} est le quatrième nombre ($x$) calculé à partir de 3 autres nombres déjà connus ($a$, $b$ et $c$).
Le tableau ci-contre est un tableau de proportionnalité.
\end{minipage}\hfill
\begin{minipage}{.23\linewidth}
\centering
\includegraphics[width=.7\linewidth]{propCours01}
\end{minipage}

On a : $\dfrac{b}{a} = \dfrac{x}{c}$

\vspace{.5em}

Et donc : 	$a \times  x = b \times  c \quad$ 	(\textbf{égalité des} \MotDefinition{produits en croix}{}).
\end{propriete}

\vspace{1em}

\begin{exemple*1}
Calcule le prix $x$ de trois baguettes grâce au tableau de proportionnalité suivant.

\correction
Le prix du pain est proportionnel au nombre de baguettes achetées.

\vspace{.5em}

\begin{center}
\renewcommand*\tabularxcolumn[1]{>{\centering\arraybackslash}m{#1}}
\begin{ctableau}{.5\linewidth}{3}
\hline
Nombre de baguette & 5 & 3 \\ \hline
Prix en € & 4,25 & $x$ ? \\ \hline
\end{ctableau}
\end{center}

L'égalité des produits en croix donne : $5 \times  x = 4,25 \times  3$.

Donc :		$x = \dfrac{4,25 \times 3}{5} = \dfrac{12,75}{5} = 2,55$\,€.
\end{exemple*1}

\vspace{3em}

\newpage

\subsection{Représentation graphique}

\vspace{1em}

\begin{propriete}
Si on représente, dans un repère, une situation de proportionnalité alors \textbf{on obtient des points alignés avec l'origine du repère}.
\end{propriete} 


\vspace{-0.25cm}
\begin{exemple*1}
Le périmètre $p$ d'un carré est proportionnel à son côté $c$ puisqu'on a $p = 4c$.
Représente graphiquement le périmètre en fonction du côté.
\correction
\begin{enumerate}
    \item On choisit des valeurs pour le côté $c$.
    \item On calcule les valeurs correspondantes du périmètre $p$.
    
    \begin{center}
        \includegraphics[width=.5\linewidth]{propCours02}
    \end{center}
    
    \item On place les points dans un repère comme ci-dessous.
    
    \begin{center}
        \includegraphics[width=.3\linewidth]{propCours03}
    \end{center}
    
\end{enumerate}
 
\end{exemple*1}


\begin{propriete}
Si une situation est représentée par des points alignés avec l'origine du repère alors \textbf{c'est une situation de} \MotDefinition{proportionnalité}{}.
\end{propriete}

\vspace{-0.25cm}
\begin{exemple*1}
Ces graphiques représentent-ils des situations de proportionnalité ? Justifie.

\begin{center}
    \includegraphics[width=\linewidth]{propCours04}
\end{center}

\correction

\vspace{1em}

\begin{minipage}[t]{.28\linewidth}
Oui, car les points sont alignés avec l'origine du repère.
\end{minipage}\hfill%
\begin{minipage}[t]{.28\linewidth}
Non, car les points sont  alignés mais pas avec l'origine du repère.
\end{minipage}\hfill%
\begin{minipage}[t]{.26\linewidth}
Non, car les points ne sont pas alignés.
\end{minipage}

\end{exemple*1}
 
\section{Applications de la proportionnalité}


\subsection{Échelle}


\begin{aconnaitre}
L'\MotDefinition{échelle}{} d'une reproduction (carte, plan, photos,maquettes) est \textbf{le coefficient de proportionnalité} qui permet de passer \textbf{des dimensions réelles} d'un objet aux \textbf{dimensions correspondantes sur sa reproduction}.

\vspace{.5em}

Pour déterminer une échelle, on calcule le quotient : $\dfrac{\text{dimension sur la reproduction}}{\text{dimension réelle correspondante}}$

\vspace{.5em}

Ces dimensions sont exprimées dans la même unité, en général le centimètre (cm).
\end{aconnaitre} 

\begin{remarque}
\begin{itemize}
    \item Si l'échelle est \textbf{inférieure à 1}, la reproduction est une \textbf{réduction}.
    \item Si l'échelle est \textbf{supérieure à 1}, la reproduction est un \textbf{agrandissement}.
\end{itemize}
\end{remarque}

L'échelle s'exprime souvent sous la forme d'une fraction avec le numérateur égal à 1 (réduction) ou le dénominateur égal à 1 (agrandissement) : $\dfrac{1}{500 000}$, $\dfrac{3}{1}$. 

On peut aussi l'écrire : 1\,:\,500 000 ou 1/500 000 ; 3\,:\,1 ou 3/1.


\begin{exemple*1}
Calculer la distance réelle correspondant à 3,5\,cm sur une carte à l'échelle $\dfrac{1}{500\,000}$ et la distance sur cette carte entre deux villes distances de 25\,km.
\correction
Échelle $\dfrac{1}{500\,000}$ signifie qu'1\,cm sur la carte représente 500\,000\,cm en réalité.


\begin{center}
\renewcommand*\tabularxcolumn[1]{>{\centering\arraybackslash}m{#1}}
\begin{ctableau}{.85\linewidth}{3}
\hline
Distance sur la carte (cm) & 1 & 3,5 \\ \hline
Distance réelle (cm) & 500 000 & $x$ \\ \hline
\end{ctableau}
\end{center}

L'égalité des produits en croix donne : $x = \dfrac{3,5 \times 500\,000}{1} = 1\,750\,000$\,cm = 17,5\,km.

\textbf{La distance réelle correspondant à 3,5\,cm sur cette carte est de 17,5\,km.}


On procède de même mais on n'oublie pas de convertir tout d'abord la distance réelle en cm : 25\,km= 2\,500\,000\,cm

\begin{center}
\renewcommand*\tabularxcolumn[1]{>{\centering\arraybackslash}m{#1}}

\begin{ctableau}{.85\linewidth}{3}
\hline
Distance sur la carte (cm) & 1 & $y$ \\ \hline
Distance réelle (cm) & 500 000 & 2 500 000 \\ \hline
\end{ctableau}
\end{center}

L'égalité des produits en croix donne : $y=\dfrac{1\times 2 500 000}{500 000}=5$\,cm.

\textbf{Sur la carte, la distance séparant les 2 villes est de 5\,cm.}
\end{exemple*1}

\begin{exemple*1}
Avec un microscope au 3000/1, on observe une cellule de 4,8\,cm de diamètre. Quel est le diamètre réel de la cellule ? Avec ce réglage, quelle serait le diamètre observé pour le noyau de 4\,$\mu$m de cette cellule?

\correction

L'échelle 3000/1 signifie qu'un centimètre en réalité correspond à 3000\,cm sur la reproduction

\begin{center}
\renewcommand*\tabularxcolumn[1]{>{\centering\arraybackslash}m{#1}}

\begin{ctableau}{.7\linewidth}{3}
\hline
Taille avec le microscope (cm) & 3 000 & 4,8 \\ \hline
Taille réelle (cm) & 1 & $x$ \\ \hline
\end{ctableau}
\end{center}

L'égalité des produits en croix donne : $x=\dfrac{1 \times 4,8}{3000}=0,0016$\,cm = 16\,$\mu$m

\textbf{Le diamètre de la cellule est de 16\,$\mu$m.}

4\,$\mu$m = 0,0004 cm

\begin{center}
\renewcommand*\tabularxcolumn[1]{>{\centering\arraybackslash}m{#1}}

\begin{ctableau}{.7\linewidth}{3}
\hline
Taille avec le microscope (cm) & 3 000 & $y$ \\ \hline
Taille réelle (cm) & 1 & 0,0004 \\ \hline
\end{ctableau}
\end{center}

L'égalité des produits en croix donne : $y=\dfrac{3000 \times 0,0004}{1}=1,2$\,cm.

\textbf{Le diamètre observé pour le noyau de la cellule est de 1,2\,cm.}
\end{exemple*1}

\newpage



