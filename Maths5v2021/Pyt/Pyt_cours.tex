
\section{Utiliser la définition de la racine carrée}

\begin{aconnaitre}
La \MotDefinition{racine carrée}{} \textbf{d'un nombre positif} $a$ est le nombre positif, noté $\sqrt{a}$, dont le carré est $a$. Le symbole $\sqrt{\text{ }}$ est appelé « \MotDefinition{radical}{} » et le nombre placé sous le radical est appelé le \MotDefinition{radicande}{}.
\end{aconnaitre}

\begin{remarque}
$\sqrt{a}$ n'a pas de sens lorsque $a$ est un nombre strictement négatif.
\end{remarque}

\begin{aconnaitre}
Pour tout nombre \textbf{positif} a, $(\sqrt{a})^2=a$ et $\sqrt{a^2}=a$. 
\end{aconnaitre}

\begin{exemple*1}
Calcule $\sqrt{1}$ ; $(\sqrt{3,6})^2$ ; $\sqrt{9}$ ; $\sqrt{5^2}$ ; $\sqrt{(-5)^2}$ ; $\sqrt{2} \times \sqrt{2}$ et $\sqrt{1,3 \times 1,3}$.

\correction
\begin{itemize}
    \item $1^2$ = 1 et 1 est positif donc $\sqrt{1} = 1$
    \item 3,6 est positif donc $(\sqrt{3,6})^2=3,6$
    \item $3^2 = 9$ et 3 est positif donc $\sqrt{9}=3$
    \item 5 est positif donc $\sqrt{5^2}=5$ et $\sqrt{(-5)^2}=5$ 
    \item 2 est positif donc $\sqrt{2} \times \sqrt{2}= (\sqrt{2})^2 = 2$
    \item 1,3 est positif donc $\sqrt{1,3 \times 1,3} = \sqrt{1,3^2} = 1,3$
\end{itemize}
\end{exemple*1}

\begin{aconnaitre}
Un \MotDefinition{carré parfait}{} est le carré d'un nombre entier. La racine carrée d'un carré parfait est donc un nombre entier positif.
\end{aconnaitre}

\begin{exemple*1}
À l'aide de la calculatrice, donne la valeur exacte ou la valeur arrondie au millième des nombres $\sqrt{625}$ ; $\sqrt{2}$ et $\sqrt{12,25}$.

\correction
On utilise la touche \touchecalc{$\sqrt{\text{ }}$} de la calculatrice.
\begin{itemize}
\item Pour $\sqrt{625}$, la calculatrice affiche 25. Donc $\sqrt{625}= 25$ (\textbf{valeur exacte}).

La racine carrée de 625 est un entier donc 625 est un \textbf{carré parfait}.

\item  Pour $\sqrt{2}$ la calculatrice affiche 1,414213562.
    
2 \textbf{n'est pas un carré parfait}, on donne une \textbf{valeur arrondie} de $\sqrt{2}$. Donc $\sqrt{2} \approx 1,414$ (\textbf{valeur arrondie au millième}).

\item Pour $\sqrt{12,25}$ la calculatrice affiche 3,5. Donc $\sqrt{12,25}= 3,5$ (\textbf{valeur exacte}).
\end{itemize}
\end{exemple*1}


\begin{exemple*1}
\begin{enumerate}
    \item Recopie et complète.
    \[ \sqrt{0}=... \quad ; \quad \sqrt{81} =... \quad ; \quad \sqrt{7,3^2}=... \quad ; \quad \sqrt{\text{...}}=4 \]
    
    \[\sqrt{\left(\dfrac{2}{3}\right)^2}=... \quad ; \quad \sqrt{\pi} \times \sqrt{\pi} =... \quad ; \quad \sqrt{\dfrac{1}{3} \times \dfrac{1}{3}} =... \]
    
    \item Calcule et donne le résultat sous forme d'un nombre décimal.
    \[ A=\sqrt{4} \quad ; \quad B=\sqrt{25} \quad ; \quad C=(-\sqrt{4,9})^2 \quad ; \quad D=\sqrt{(-7)^2} \quad ; \quad E=\left(\dfrac{1}{\sqrt{5}}\right)^2 \]
    \item À l'aide de la calculatrice, donne l'écriture décimale exacte ou approchée à 0,001 près par défaut des nombres.
    \[ F=\sqrt{3} \quad ; \quad G=\dfrac{\sqrt{529}}{23} \quad ; \quad H=5\sqrt{0,81} \quad ; \quad I=\sqrt{3+\dfrac{2}{3}} \quad ; \quad J=\dfrac{\sqrt{3}-1}{1+\sqrt{5}} \]
    \item Dresse la liste des douze premiers carrés parfaits.
\end{enumerate}
\end{exemple*1}


\vspace{2em}

\section{Théorème de Pythagore}
           
\vspace{1em}

\begin{theoreme}[\MotDefinition{Théorème de Pythagore}{}]

Si \textbf{un triangle est rectangle} alors \textbf{le carré de la longueur de l'hypoténuse est égal à la somme des carrés des longueurs des deux autres côtés}.
\end{theoreme}

\vspace{2em}

\begin{exemple*1}
Le triangle ci-dessous est rectangle donc $a^2 = b^2 + c^2$.

\begin{center}
    \includegraphics[width=.3\linewidth]{pythCours01}
\end{center}
\end{exemple*1}

\newpage

\begin{exemple*1}
Calcul de la longueur d'un côté de l'angle droit.

Soit $RAS$ un triangle rectangle en $A$ tel que $RS = 9,7$\,cm et $RA = 7,2$\,cm. Calculer $AS$.

\correction

\begin{minipage}{.28\linewidth}
Figure à main levée :
\centering
\includegraphics[width=.8\linewidth]{pythCours02}
\end{minipage}\hfill%
\begin{minipage}{.68\linewidth}
Le triangle $RAS$ est rectangle en $A$, son hypoténuse est le côté $[RS]$. Donc, d'après le théorème de Pythagore, on a :
\begin{align*}
RS^2 &= RA^2 + AS^2 \\
9,72 &= 7,22 + AS^2 \\     
AS^2 &= 9,72 - 7,22 \\
AS^2 &= 94,09 - 51,84 \\
AS^2 &= 42,25 \\
\end{align*}
\end{minipage}

La longueur $AS$ est positive donc $AS =\sqrt{42,25}$\,cm.

(La valeur obtenue avec la calculatrice pour $\sqrt{42,25}$ est 6,5. C'est une valeur exacte car $6,5^2 = 42,25$.)

Donc $AS = 6,5$\,cm (\textbf{valeur exacte}).
\end{exemple*1}

\vspace{4em}

\begin{exemple*1}
$NUL$ est un triangle tel que $NU = 42$\,cm ; $LU = 46$\,cm et $LN = 62$\,cm.

Démontrer que $NUL$ n'est pas un triangle rectangle.

\correction
Remarque préliminaire : Si ce triangle est rectangle, seul le côté $[LN]$ peut être son hypoténuse car c'est le côté le plus long.

Dans le triangle $NUL$, le plus long côté est $[LN]$ donc on calcule séparément $LN^2$ et $LU^2 + NU^2$ :

\begin{minipage}[t]{.48\linewidth}
D'une part,	
\begin{align*}
    LN^2 &= 62^2 \\ 
	LN^2 &= 3844 \\
\end{align*}

\end{minipage}\hfill%
\begin{minipage}[t]{.48\linewidth}
D'autre part,
\begin{align*}
   	LU^2 + NU^2 &= 46^2 + 42^2 \\
	LU^2 + NU^2 &= 2116 + 1764 \\
	LU^2 + NU^2 &= 3880 \\
\end{align*}
\end{minipage}

On constate que $LN^2 \neq LU^2 + NU^2$. Or si le triangle était rectangle, d'après le théorème de Pythagore, il y aurait égalité. Comme ce n'est pas le cas, le triangle $NUL$ n'est pas rectangle.
\end{exemple*1}



