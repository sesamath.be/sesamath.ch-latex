



\QCMautoevaluation{Pour chaque question, plusieurs réponses sont
  proposées.  Déterminer celles qui sont correctes.} % Est-ce que c'est toujours le cas ?

\begin{QCM}
  \begin{GroupeQCM}
  
  \begin{center}
%\definecolor{cqcqcq}{rgb}{0.752941176471,0.752941176471,0.752941176471}
\begin{tikzpicture}[scale=0.65][line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
\draw [color=A3,dash pattern=on 3pt off 3pt, xstep=1.0cm,ystep=1.0cm] (-3.94,-1.12) grid (3.88,8.3);
\draw[->,color=black] (-3.94,0.) -- (3.88,0.);
\foreach \x in {-3.,-2.,-1.,1.,2.,3.}
\draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[below] {\footnotesize $\x$};
\draw[->,color=black] (0.,-1.04) -- (0.,8.3);
\foreach \y in {-1.,1.,2.,3.,4.,5.,6.,7.,8.}
\draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt) node[left] {\footnotesize $\y$};
\draw[color=black] (0pt,-10pt) node[right] {\footnotesize $0$};
\clip(-3.94,-1.12) rectangle (3.88,8.3);
\draw [domain=-3.94:3.88] plot(\x,{(--2.-1.*\x)/1.});
\draw[smooth,samples=100,domain=-3.940000000000001:3.880000000000002] plot(\x,{(\x)^(2.0)});
\begin{scriptsize}
\draw[color=black] (-3.78,5.56) node {$C_f$};
\draw[color=black] (-2.62,8.16) node {$C_g$};
\end{scriptsize}
\end{tikzpicture}
\end{center}

    \begin{exercice}
      $f$ est une fonction
      \begin{ChoixQCM}{3}
      \item affine
      \item linéaire
      \item du second degré
      \end{ChoixQCM}
\begin{corrige}
     \reponseQCM{a} % ici deux réponses justes
   \end{corrige}
    \end{exercice}
    
        \begin{exercice}
      $g$ est une fonction
      \begin{ChoixQCM}{3}
      \item affine
      \item linéaire
      \item du second degré
      \end{ChoixQCM}
\begin{corrige}
     \reponseQCM{c} % ici deux réponses justes
   \end{corrige}
    \end{exercice}
    
        \begin{exercice}
      Le zéro de $f$
      \begin{ChoixQCM}{3}
      \item est 0
      \item est 2
      \item n'existe pas
      \end{ChoixQCM}
\begin{corrige}
     \reponseQCM{b} % ici deux réponses justes
   \end{corrige}
    \end{exercice}
    
      \begin{exercice}
      Le zéro de $g$
      \begin{ChoixQCM}{3}
      \item est 0
      \item est 2
      \item n'existe pas
      \end{ChoixQCM}
\begin{corrige}
     \reponseQCM{a}
   \end{corrige}
    \end{exercice}

  \begin{exercice}
      La ou les pré image(s) de 4 par $f$
      \begin{ChoixQCM}{3}
      \item est -2
      \item sont -2 et 2
      \item n'existe pas
      \end{ChoixQCM}
\begin{corrige}
     \reponseQCM{a}
   \end{corrige}
    \end{exercice}
    
      \begin{exercice}
      La ou les pré image(s) de 4 par $g$
      \begin{ChoixQCM}{3}
      \item est -2
      \item sont -2 et 2
      \item n'existe pas
      \end{ChoixQCM}
\begin{corrige}
     \reponseQCM{b}
   \end{corrige}
    \end{exercice}
    
      \begin{exercice}
       $f$ est 
      \begin{ChoixQCM}{3}
      \item croissante
      \item décroissante
      \item constante
      \end{ChoixQCM}
\begin{corrige}
     \reponseQCM{b}
   \end{corrige}
    \end{exercice}
    
      \begin{exercice}
       $g$ est 
      \begin{ChoixQCM}{3}
      \item négative
      \item positive
      \item négative puis positive
      \end{ChoixQCM}
\begin{corrige}
     \reponseQCM{b}
   \end{corrige}
    \end{exercice}

\end{GroupeQCM}
\end{QCM}

  