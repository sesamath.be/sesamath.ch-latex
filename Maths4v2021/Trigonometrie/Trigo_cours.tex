\section{Un peu d'histoire...}
\begin{historique}
\begin{minipage}{0.6\linewidth}

La trigonométrie est née de l'étude de l'astronomie, en particulier de la volonté de vouloir calculer la distance de la Terre au Soleil, et aux autres étoiles afin d'aider à la navigation. 

\vspace{0.5cm}

Hipparque, astronome grec du IIème siècle avant J.C. fut le premier à calculer des tables de cosinus, sinus et tangentes. Ptolémée d'Alexandrie, grec, au IIeme siècle après J.C., commence à étudier les propriété mathématiques de fonctions trigonométriques. 

\end{minipage}
\hfill
\begin{minipage}{0.35\linewidth}
\begin{center}
\includegraphics[scale=1.25]{Trigonometrie/figures/Ptolemee.eps}


Ptolémée
\end{center}
\end{minipage}

\vspace{0.5cm} 

Il faut toutefois attendre le XIème siècle pour que la trigonométrie soit étudiée en dehors de l'astronomie, avec notamment les mathématiciens arabes Omar Khayyam et Al-Kashi.

\vspace{0.5cm}

Aujourd'hui la trigonométrie est omni-présente en science, et notamment en électronique, en acoustique et bien sûr toujours en astronomie.

\end{historique}

\section{Définitions}

\begin{definitions}
Soit un triangle rectangle et $\alpha$ un des deux angles non-droits du triangle. On rappelle que l'hypoténuse est le plus long côté du triangle (opposé à l'angle droit). On appelle côté adjacent le second côté formant $\alpha$, avec l'hypoténuse, et on appelle côté opposé le dernier côté du triangle ("en face" de $\alpha$).
\end{definitions}

\hspace{-0.6cm}
\begin{minipage}{0.54\linewidth}
\begin{exemple*1}
Sur l'exemple ci-contre, l'hypoténuse est $[BC]$, le côté adjacent à $\alpha$ est $[AB]$, tandis que le côté opposé à $\alpha$ est $[AC]$. 
\end{exemple*1}

\begin{remarque} On remarque que le côté adjacent à $\alpha$ est le côté opposé à $\beta$, et réciproquement. \end{remarque}

\end{minipage}
\hfill
\begin{minipage}{0.45\linewidth}
\begin{center}
\input{Trigonometrie/figures/Cours01.tex}
\end{center}
\end{minipage}

\newpage

\begin{definition}
Dans un triangle rectangle, soit $\alpha$ un des angles non-droits. On définit alors le cosinus de $\alpha$ : 
\begin{equation*}
cos(\alpha) = \dfrac{adjacent}{hypotenuse}
\end{equation*}
\end{definition}

\hspace{-0.6cm}
\begin{minipage}{0.54\linewidth}
\begin{exemple*1}
En reprenant la figure précédente, si l'on a , $AC = 4$ et $BC = 5$, on obtient pour l'angle $\alpha$ tracé : 
\begin{equation*}
cos(\alpha) = \dfrac{adjacent}{hypotenuse} = \dfrac{AB}{BC} = \dfrac{3}{5}  
\end{equation*}
\end{exemple*1}
\end{minipage}
\hfill
\begin{minipage}{0.45\linewidth}
\begin{center}
\input{Trigonometrie/figures/Cours02.tex}
\end{center}
\end{minipage}

\begin{remarques} \textcolor{white}{abc}
\begin{itemize}
    \item A partir de la valeur d'un cosinus on peut aisément retrouver la valeur de l'angle en degré, en utilisant la fonction de la calculatrice $cos^{-1}$, (parfois nommée $Arccos$).
    \item On remarque que $0 \leq cos(\alpha) \leq 1$.
\end{itemize}

\end{remarques}

\section{Méthodes}

\subsection{Détermination de la valeur d'un angle}

\begin{methode}
\begin{minipage}{0.54\linewidth}
Pour déterminer la valeur de $\alpha$, en connaissant la longueur de son côté adjacent et de son hypoténuse, on commence par calculer son cos : \begin{equation*}
    cos(\alpha)=\dfrac{adjacent}{hypotenuse} = \dfrac{AB}{BC}
\end{equation*}

Puis, à l'aide de la calculatrice, on détermine $Arccos$ du cosinus calculé. Le résultat affiché est l'angle recherché

\end{minipage}
\hfill
\begin{minipage}{0.45\linewidth}
\begin{center}
\input{Trigonometrie/figures/Cours02.tex}
\end{center}
\end{minipage}

\exercice
Sur la figure précédente, considérons que $AB=5$ et $BC=9$. Déterminons alors $\alpha$.

\correction
On a alors :
\begin{equation*}
cos(\alpha)=\dfrac{adjacent}{hypotenuse} = \dfrac{AB}{BC} = \dfrac{5}{9}    
\end{equation*}
On calcule donc $\alpha = Arccos(\dfrac{5}{9}) \simeq 56,25\degres$. 

\end{methode}

\subsection{Détermination de la longueur d'un côté du triangle}
\vspace{-0.5cm}
\begin{methode}[Détermination du côté adjacent]
\begin{minipage}{0.54\linewidth}
Pour déterminer la longueur du côté adjacent $AB$, en connaissant la longueur de l'hypoténuse $BC$ et la valeur de l'angle $\alpha$, on commence par calculer $cos(\alpha)$, et on pose : \begin{equation*}
\begin{split} 
& \hspace{0.45cm} cos(\alpha)=\dfrac{AB}{BC} \\
\vspace{0.2cm}
& \Leftrightarrow AB = cos(\alpha) \times BC  \\
\end{split}
\end{equation*}
\end{minipage}
\hfill
\begin{minipage}{0.45\linewidth}
\begin{center}
\input{Trigonometrie/figures/Cours02.tex}
\end{center}
\end{minipage}

\exercice
Sur la figure précédente, considérons que $\alpha=30 \degres$, et $AB=7$. Déterminons alors $AB$.

\correction
On obtient à l'aide de la calculatrice que $cos(30\degres) \simeq 0,866$. On a alors :
\begin{equation*}
\begin{split} 
& \hspace{0.45cm} cos(\alpha)=\dfrac{AB}{BC} \\
\vspace{0.2cm}
& \Leftrightarrow AB = cos(\alpha) \times BC  \\
& \Leftrightarrow AB \simeq 0,866 \times 7 \\
& \Leftrightarrow AB \simeq 6,06
\end{split}
\end{equation*}

\end{methode}

\begin{methode}[Détermination de l'hypoténuse]
\begin{minipage}{0.54\linewidth}
Pour déterminer la longueur de l'hypoténuse $BC$, en connaissant la longueur du côté adjacent $AB$ et la valeur de l'angle $\alpha$, on commence par calculer $cos(\alpha)$, et on pose : \begin{equation*}
\begin{split} 
& \hspace{0.45cm} cos(\alpha)=\dfrac{AB}{BC} \\
\vspace{0.2cm}
& \Leftrightarrow BC = \dfrac{AB}{cos(\alpha)}\\
\end{split}
\end{equation*}
\end{minipage}
\hfill
\begin{minipage}{0.45\linewidth}
\begin{center}
\input{Trigonometrie/figures/Cours02.tex}
\end{center}
\end{minipage}

\exercice
Sur la figure précédente, considérons que $\alpha=45 \degres$, et $AB=3$. Déterminons alors $BC$.

\correction
On obtient à l'aide de la calculatrice que $cos(45\degres) \simeq 0,707$. On a alors :
\begin{equation*}
\begin{split} 
& \hspace{0.45cm} cos(\alpha)=\dfrac{AB}{BC} \\
\vspace{0.2cm}
& \Leftrightarrow BC = \dfrac{AB}{cos(\alpha)}\\
& \Leftrightarrow BC \simeq \dfrac{3}{0,707} \\
& \Leftrightarrow BC \simeq 4,24
\end{split}
\end{equation*}

\end{methode}

\begin{remarque}
On remarque ainsi qu'en combinant le théorème de Pythagore et la trigonométrie, on peut déduire tous les angles et longueurs d'un triangle rectangle en connaissant un angle et une longueur, ou deux des longueurs. Nous généraliserons ces résultats en 3eme puis en 2nde.
\end{remarque}


\vfill


\begin{autoeval}
\begin{enumerate}
        \begin{minipage}{0.7\linewidth}    
    \item Quel est le côté adjacent à l'angle $\alpha$?

    \begin{center}
        \begin{multicols}{3}
            \begin{enumerate}
                \item AB
                \item AC
                \item BC
            \end{enumerate}
        \end{multicols}
    \end{center}
    \end{minipage}
    \hfill
    \begin{minipage}{0.3\linewidth}
    \begin{center}
        \begin{adjustbox}{width=0.75\linewidth}
            \input{Trigonometrie/figures/AutoEval01}        
        \end{adjustbox}
    \end{center}
    \end{minipage}
    

    \item Sur le triangle plus haut, si $AB=10$, et $\alpha=60\degres$, que vaut $BC$ ?

    \begin{center}
        \begin{multicols}{4}
            \begin{enumerate}
                \item 5 
                \item 15
                \item 20
            \end{enumerate}
        \end{multicols}
    \end{center}
    
    \item  Sur le triangle plus haut, si $AB=5$, $AC=12$ et $BC=13$. Que vaut alors $\alpha$, arrondi au degré ? 
    \begin{center}
        \begin{multicols}{4}
            \begin{enumerate}
                \item $23\degres$
                \columnbreak
                \item $65\degres$
                \columnbreak
                \item $67\degres$
            \end{enumerate}
        \end{multicols}
    \end{center}
\end{enumerate}


\begin{flushright}
\rotatebox{180}{Si tu as 3 réponses, choisis le chemin rouge. Si tu en as 2, le chemin jaune. Sinon, choisis le chemin vert.}
\rotatebox{180}{\textbf{Les solutions sont} : a-c-c.}
\end{flushright}
\vspace{-1cm}

\end{autoeval}