
\section{Un peu d'histoire...}

\begin{historique}
Lors de la Renaissance, entre les $XIV^e$ et $XVI^e$ siècles, l'Europe redécouvre les grands classiques de l'Antiquité, et les mathématiques ne sont pas en reste : en effet, on redécouvre notamment les écrits de Diophante le grec et Al-Khwarizmi le perse, qui furent les premiers à désigner des inconnues par des mots, et à détacher les problèmes mathématiques de la géométrie. 

\vspace{0.5cm}


\begin{minipage}{0.6\linewidth}
Dans cet optique se crée alors l'algèbre nouvelle, qui remplace l'algèbre rhétorique, la Renaissance étant également la période d'accès en masse à la connaissance, via notamment l'invention de l'impression par Gutenberg en 1450. 

\vspace{0.5cm}

On écrit alors les expressions et équations avec des lettres, notamment sous l'impulsion de François Viète, mathématicien français du $XVI^e$ siècles. Le texte devient ainsi moins lourd et beaucoup plus lisible. 

\vspace{0.5cm}

Ce mouvement de ré-écriture des mathématiques est achevé par René Descartes, au siècle suivant, qui donnât la forme moderne d'écrire les mathématiques.

\end{minipage}
\hfill
\hspace{0.2cm}
\begin{minipage}{0.4\linewidth}



\begin{center}
\includegraphics[scale=0.65]{CalculLitteral/figures/Viete.eps}

François Viète
\end{center}
\end{minipage}

\end{historique}

\section{Définitions}

\subsection{Monômes et polynômes}
\begin{definitions}
\begin{itemize}
    \item On appelle monôme toute expression de la forme $ax^n$, où $a$ est appelé coefficient du monôme et $n$ le degré du monôme.
    \item On appelle polynôme toute somme de monôme de différents degrés. On appelle degré du monôme le plus grand degré des monômes.
\end{itemize}
\end{definitions}

\begin{exemple*1}

\begin{itemize}
    \item $3x^4$ est un monôme, de coefficient $3$ et de degré $4$.
    \item $4x^3+\dfrac{4}{3}x^2-7$ est un polynôme de degré $3$.
\end{itemize}
\end{exemple*1}

\begin{remarque}
On appelle un polynôme qui est composé de 2 monôme un binôme, et s'il est composé de 3 monôme, on parle alors de trinôme.
\end{remarque}


\subsection{Factorisation et développement}
\begin{definitions}
\begin{itemize}
    \item On dit qu'on factorise une expression lorsque l'on transforme une somme en un produit.
    \item On dit qu'on développe une expression lorsque l'on transforme un produit en une somme.
\end{itemize}
\end{definitions}

\begin{exemple*1}

\begin{itemize}
    \item Passer de $3x + 3y$ à $3(x+y)$, c'est factoriser.
    \item Passer de $2(x+3)$ à $2x+6$, c'est développer.
\end{itemize}
\end{exemple*1}

\section{Distributivité simple}

\begin{propriete} Soit $a$,$b$ et $k$ des expressions littérales.
On a alors $k \times (a+b) = k \times a + k \times b $. Passer de gauche à droite est développer, dans le sens inverse il s'agit de factoriser.
\end{propriete}

\begin{exemple*1}

\begin{itemize}
    \item $2x(4x+3) = 2x \times 4x + 2x \times 3 = 8x^2 + 6x$. On a développé.
    \item $3x(x+3) - (x+3) = (3x-1)(x+3)$. On a factorisé.
\end{itemize}
\end{exemple*1}

\section{Double distributivité}

\begin{propriete}
Soit $a$, $b$, $c$, $d$ des expressions littérales.
On a alors : 
\begin{equation*}
(a+b)(c+d) = ac + ad + bc + bd
\end{equation*}
\end{propriete}

\begin{exemple*1}
On a :
\begin{equation*}
    \begin{split}
        & (2x+1)(x-2) = 2x \times x + 2x \times (-2) + 1 \times x + 1\times (-2) \\
        & \hspace{2.34cm} = 2x^2-4x+x-2 \\
        & \hspace{2.34cm} = 2x^2-3x-2 \\
    \end{split}
\end{equation*}

\end{exemple*1}

\section{Identités remarquables}

\begin{theoreme}[Identités Remarquables]
Soit $a$ et $b$ des expressions littérales. On a :
\begin{itemize}
    \item $(a+b)^2=a^2+2ab+b^2$
    \item $(a-b)^2=a^2-2ab+b^2$
    \item $(a+b)(a-b)=a^2-b^2$
\end{itemize}

\end{theoreme}

\begin{remarques}  \\
\begin{itemize}
    \item On retrouve ces égalités en appliquant la double distributivité.
    \item On se servira de ces identités à la fois pour développer et pour factoriser (en particulier la 3ème).
\end{itemize}
\end{remarques}

\begin{exemple*1}  \\
\begin{itemize}
    \item $(x+3)^2 = x^2 + 2\times x \times 3 + 3^2 = x^2 + 6x + 9$. 
    
    On a appliqué ici la première identité remarquable avec $a=x$ et $b=3$.
    
    \vspace{0.5cm}
    
    \item $(2x-1)^2 = (2x)^2 - 2\times 2x \times 1 + 1^2= 4x^2 -4x +1$. 
    
    On a appliqué ici la deuxième identité remarquable avec $a=2x$ et $b=1$. 
    
    \vspace{0.5cm}
    
    \item $x^2-16 = x^2 - 4^2 = (x+4)(x-4)$. 
    
    On a appliqué ici la troisième identité remarquable avec $a=x$ et $b=4$. 
\end{itemize}
\end{exemple*1}



\begin{autoeval}
\begin{enumerate}
    \item On est passé de $3x+3y$ à $3(x+y)$. On a alors...

    \begin{center}
        \begin{multicols}{4}
            \begin{enumerate}
                \item Calculer
                \item Développer
                \item Factoriser
                \item Une autre réponse
            \end{enumerate}
        \end{multicols}
    \end{center}

    \item Laquelle de ces affirmations est vraie ?
        \begin{center}
        \begin{multicols}{3}
            \begin{enumerate}
                \item $(x+1)^2 = x^2 + 1$
                \columnbreak
                \item $(x-1)^2 = x^2 - 1$
                \columnbreak
                \item $(x-1)(x+1) = x^2 - 1$
                \columnbreak
            \end{enumerate}
        \end{multicols}
    \end{center}
    
    \item $(-x+1)(-2x+3) = ...$
    \begin{center}
        \begin{multicols}{4}
            \begin{enumerate}
                \item $-3x+3$
                \columnbreak
                \item $-2x^2-5x+3$
                \columnbreak
                \item $2x^2+3$
                \columnbreak
                \item Une autre réponse
            \end{enumerate}
        \end{multicols}
    \end{center}
\end{enumerate}


\begin{flushright}
\rotatebox{180}{Si tu as 3 réponses, choisis le chemin rouge. Si tu en as 2, le chemin jaune. Sinon, choisis le chemin vert.}
\rotatebox{180}{\textbf{Les solutions sont} : c-c-d.}
\end{flushright}
\vspace{-1cm}

\end{autoeval}