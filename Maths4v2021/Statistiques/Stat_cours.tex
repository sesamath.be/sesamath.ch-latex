\section{Un peu d'histoire...}

\begin{historique}
Il est évident que les statistiques furent utilisées de tous temps : on peut citer les recensements égyptiens ou chinois aux XXVIIIème et XXIIIème siècles av. J.C.

\vspace{0.5cm}

\begin{minipage}{0.6\linewidth}
Toutefois la mathématisation de tels calculs fut assez tardive, et on cite souvent le livre "Essai sur la probabilité de durée de vie humaine" d'Antoine Deparcieux en 1746 comme le premier traité de statistiques, à l'époque dans le but de pouvoir créer des assurances-vie.

\vspace{0.25cm}

On peut ensuite noter les travaux de Laplace en 1802 qui se sert de statistiques pour estimer la population francaise.
\end{minipage}
\hfill
\begin{minipage}{0.4\linewidth}
\begin{center}
    \includegraphics[scale=0.75]{Statistiques/figures/Deparcieux.eps}
    
    Antoine Deparcieux
\end{center}
\end{minipage}
\end{historique}


\section{Moyenne}

\begin{notations}
On considère une série statistique, représentée par le tableau suivant : 

\begin{center}

\begin{tabular}{|c||c|c|c|c|}
\hline
\textbf{Valeur du caractère} & $x_1$ & $x_2$ & ... & $x_p$\\
\hline
\textbf{Effectif} & $n_1$ & $n_2$ & ... & $n_p$ \\
\hline
\end{tabular}

\end{center}

Ce qui signifie qu'il y a $n_1$ valeur valant $x_1$, etc...
On note $N$ l'effectif total. 
\end{notations}

\begin{definition} On appelle moyenne de la série statistique le nombre : 

\begin{equation*}
    M = \dfrac{x_1\times n_1+x_2\times n_2+...+x_p\times n_p}{N}.
\end{equation*}
\end{definition}

\begin{exemple*1}  Suite à un contrôle, voici les résultats d'une classe : 

\begin{center}
\begin{tabular}{|c||c|c|c|c|c|c|c|c|}
\hline
\textbf{Valeur du caractère} & 2 & 6 & 8 & 10 & 11 & 13 & 14 & 17\\
\hline
\textbf{Effectif} & 1 & 3 & 2 & 6 & 4 & 3 & 4 & 1 \\
\hline
\end{tabular}

\end{center}
 L'effectif total est alors de $N= 1+3+2+6+4+3+4+1 = 24$.
 La moyenne est alors de : $\dfrac{2\times 1+6\times 3 + 8\times 2 + 10\times 6 + 11\times 4 + 13\times 3 + 14\times 4 + 17\times 1}{24} = \dfrac{252}{24} = 10,5$. La moyenne de la classe sur ce contrôle est donc de 10,5.
 \end{exemple*1}

\begin{remarques} \textcolor{white}{abc}
\begin{itemize}
    \item Dans le cas d'une moyenne pondérée, c'est-à-dire avec des coefficients, comme par exemple une moyenne scolaire, les $n_i$ jouent le rôle des coefficients. 
    \item Dans le cas de la moyenne de valeurs sans coefficient, on peut se contenter de faire la somme des valeurs et de diviser par le nombre de valeurs.
\end{itemize}
\end{remarques}

\newpage

\section{Autres valeurs particulières}

\subsection{Etendue}

\begin{definition} On appelle étendue de la série statistique la différence entre le plus haut terme et le plus bas terme de la série. \end{definition}

\begin{exemple*1} Suite à un contrôle dans une classe, la meilleure note fut de 17, la pire de 5. L'étendue de la série statistique est alors $17-5 = 12$. \end{exemple*1}

\subsection{Médiane}

\begin{definition} On appelle médiane de la série toute valeur $m$ permettant de couper la série statistique en deux parties d'effectif égaux.

\begin{center}
\includegraphics[scale=0.8]{Statistiques/figures/Mediane.eps}
\end{center}
\end{definition}

\begin{proprietes} Pour calculer la médiane d'une série statistique ordonnée, on distingue deux cas : \begin{itemize}
        \item Si $N$ est impair, la médiane est la $\dfrac{N+1}{2}^{eme}$ valeur de la série statistique
        \item Si $N$ est pair, la médiane est n'importe quelle valeur comprise entre la $\dfrac{N}{2}^{eme}$ valeur et la $(\dfrac{N}{2}+1)^{eme}$ valeur. On prendra tradionnellement la moyenne entre ces deux valeurs.
    \end{itemize}
\end{proprietes}

\begin{exemple*1} Considérons la série statistique suivante : 
    
    \begin{center}
\begin{tabular}{|c||c|c|c|c|c|c|c|}
\hline
\textbf{Valeur du caractère} & 2 & 5 & 7 & 8 & 9 & 11\\
\hline
\textbf{Effectif} & 1 & 3 & 2 & 2 & 1 & 2 \\
\hline
\end{tabular}
    \end{center}
    
Ici l'effectif total est de $N = 1+3+2+2+1+2 =11$, qui est impair. La médiane est donc la $\dfrac{N+1}{2}^{eme}$ valeur de la série, soit la $\dfrac{11+1}{2} = 6^{eme}$ valeur, qui est 7. La médiane de la série est donc 7.
\end{exemple*1}

\begin{remarque} Dans le cas où l'effectif total est pair, plusieurs valeurs peuvent convenir pour la médiane : elle n'est pas unique. 
Par exemple, dans la série statistique : 2-3-5-8-9-10, n'importe quelle valeur entre 5 et 8 convienne comme médiane, même si on prendra tradionnellement 6,5.\end{remarque}

\subsection{Quartiles}

\begin{definitions}  \begin{itemize}
    \item On appelle premier quartile la plus petite valeur $q_1$ de la série telle que au moins 25\% des valeurs de la série sont inférieures ou égales à $q_1$.
    \item On appelle troisième quartile la plus petite valeur $q_3$ de la série telle que au moins 75\% des valeurs de la série sont inférieures ou égales à $q_3$.
\end{itemize}
\end{definitions}
\begin{center}
\includegraphics[scale = 1.5]{Statistiques/figures/quartiles.eps}
\end{center}

\begin{proprietes} \begin{itemize}
    \item Pour calculer le premier quartile, on considère $\dfrac{N}{4}$. On doit avoir au moins 25\% de la série statistique, on arrondit donc $\dfrac{N}{4}$ à la valeur supérieure, puis on prends le terme correspondant.
    \item On fait de même pour calculer le troisième quartile, mais en considérant $\dfrac{3N}{4}$.
\end{itemize}
\end{proprietes}

\begin{exemple*1}

Soit la série statistique suivante : 

\begin{center}

\begin{tabular}{|c|c|c|c|c|c|c|}
\hline
Valeur du caractère & 12 & 19 & 24 & 32 & 37 & 46\\
\hline
Effectif & 1 & 2 & 3 & 1 & 1 & 1 \\
\hline
\end{tabular}

\end{center}

L'effectif total ici est de $N = 1+2+3+1+1+1 = 9$. 
Pour le premier quartile, on calcule donc $\dfrac{N}{4} = 2,25$ que l'on arrondit à 3. Le premier quartile est donc la $3^{eme}$ valeur de la série, c'est à dire 19.
Pour le troisième quartile, on calcule $\dfrac{3N}{4} = 6,75$, que l'on arrondit à 7. Le troisième quartile est donc la $7^{eme}$ valeur de la série, c'est à dire 32.
\end{exemple*1}

\newpage

\begin{autoeval}
\begin{enumerate}
    \item Laquelle de ces affirmations est fausse ?
        \begin{center}
        \begin{multicols}{3}
            \begin{enumerate}
                \item La moyenne n'est pas sensible aux valeurs extrêmes.
                \columnbreak
                \item La médiane divise la série en deux groupes de tailles égales.
                \columnbreak
                \item Les quartiles sont toujours des valeurs de la série.
                \columnbreak
            \end{enumerate}
        \end{multicols}
    \end{center}
    

    \item On lit dans la presse, la phrase suivante : "Un quart des élèves travaillent moins de 30mns le week-end". 
    A quel indicateur statistique corresponds la valeur de 30mns ?

    \begin{center}
        \begin{multicols}{4}
            \begin{enumerate}
                \item La moyenne 
                \item La médiane
                \item Le premier quartile
                \item Le maximum
            \end{enumerate}
        \end{multicols}
    \end{center}
    
    \item On considère la valeur statistique suivante : 1-1-2-2-5-7. Laquelle de ces affirmations est fausse ?
    \begin{center}
        \begin{multicols}{4}
            \begin{enumerate}
                \item La moyenne est de 3.
                \columnbreak
                \item La médiane est de 3.
                \columnbreak
                \item L'étendue est de 6.
                \columnbreak
                \item Le 3ème quartile est de 5.
            \end{enumerate}
        \end{multicols}
    \end{center}
\end{enumerate}


\begin{flushright}
\rotatebox{180}{Si tu as 3 réponses, choisis le chemin rouge. Si tu en as 2, le chemin jaune. Sinon, choisis le chemin vert.}
\rotatebox{180}{\textbf{Les solutions sont} : a-c-b.}
\end{flushright}
\vspace{-1cm}

\end{autoeval}