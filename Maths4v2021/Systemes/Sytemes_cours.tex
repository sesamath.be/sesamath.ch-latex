\begin{historique}

En Chine antique, l'étude de l'astronomie est prévalente et motive beaucoup les recherches mathématiques : pour ceci, on s'intéresse à la résolution de systèmes d'équations particuliers, dont les solutions sont entières. Un exemple en est : 

\vspace{0.5cm}

\begin{minipage}{0.6\linewidth}
\textit{"Soient des objets en nombre inconnu. Si on les range par 3 il en reste 2. Si on les range par 5, il en reste 3 et si on les range par 7, il en reste 2. Combien a-t-on d'objets ?"}

\vspace{0.5cm}

Ce genre de problèmes fut beaucoup étudié jusqu'à ce qu'en 1247, Qin Jiushao publie dans son ouvrage Shùshū Jiǔzhāng (« Traité mathématique en neuf chapitres ») une solution générale à ce genre de problèmes, que l'on appelle encore aujourd'hui dans le monde francophone le théorème des restes chinois.
\end{minipage}
\hfill
\begin{minipage}{0.4\linewidth}
\begin{center}
    \includegraphics[scale=0.85]{Systemes/figures/Jiushao.eps}
    
    Qin Jiushao
\end{center}
\end{minipage}
\end{historique}

\section{Résolution d'un système}
\begin{definition} On appelle système d'équations tout ensemble d'équations liées entre elles.   \end{definition}

\begin{remarque}
En 4eme, on étudiera principalement les systèmes de 2 équations et à 2 inconnues. Ceci signifie que l'on cherchera à déterminer deux réels, généralement notés $x$ et $y$, à l'aide de 2 équations liées entre elles. Toutefois, les systèmes peuvent avoir autant d'équations et d'inconnues que l'on veut.
\end{remarque}

\begin{exemple*1} Voici un exemple de système de 2 équations à 2 inconnues : 
\begin{center}
$~~~~\left\{
\begin{array}{l}
x+2y = 4 \\
x-y = 1
\end{array}
\right.$
\end{center}

\end{exemple*1}

\begin{proprietes} Il y a deux facons "classiques" de résoudre un système d'équation.
\begin{itemize}
    \item \underline{\textbf{Par substitution :}} On ré-écrit la première équation afin d'exprimer $x$ en fonction de $y$. On remplace alors $x$ dans la deuxième équation par son expression en fonction de $y$, on résouds alors la seconde équation qui ne contient plus que $y$ comme inconnue, pour trouver $y$ et en déduire $x$.
    \item \underline{\textbf{Par combinaison :}} On additionne ou on soustrait la deuxième équation à la première (éventuellement plusieurs fois), jusqu'à n'avoir plus qu'une inconnue dans la première ligne du système. On trouve alors une inconnue et on en déduit l'autre.
    \end{itemize}
\end{proprietes}

\begin{remarque} Bien évidemment, on peut combiner ces deux méthodes, ou les adapter. \end{remarque}

\begin{exemple*1} On va résoudre le système précédent des deux facons décrites. 
\begin{itemize}
    \item \underline{\textbf{Par substitution :}} 
  \begin{multicols}{2}  
    
    $~~~~\left\{
\begin{array}{l}
x+2y = 4 \\
x-y = 1
\end{array}
\right.$ \\
  
 $\Leftrightarrow
 \left\{
\begin{array}{l}
x = 4-2y \\
x-y = 1
\end{array}
\right.$\\
  
  $\Leftrightarrow
  \left\{
\begin{array}{l}
x = 4-2y \\
(4-2y)-y = 1
\end{array}
\right.$\\

$\Leftrightarrow
\left\{
\begin{array}{l}
x = 4-2y \\
4-3y = 1
\end{array}
\right.$\\

$\Leftrightarrow
\left\{
\begin{array}{l}
x = 4-2y \\
-3y = -3
\end{array}
\right.$\\

$\Leftrightarrow
\left\{
\begin{array}{l}
x = 4-2y \\
y = 1
\end{array}
\right.$\\

$\Leftrightarrow
\left\{
\begin{array}{l}
x = 4-2 \times 1 \\
y = 1
\end{array}
\right.$\\

$\Leftrightarrow
\left\{
\begin{array}{l}
x = 2 \\
y = 1
\end{array}
\right.$\\
    
\end{multicols}
    La solution du système est donc le couple (2;1).
    
    \item \underline{\textbf{Par combinaison :}}
    \begin{multicols}{2}  
    
    $~~~~\left\{
\begin{array}{l}
x+2y = 4 \\
x-y = 1
\end{array}
\right.$ \\
  
 $\Leftrightarrow
 \left\{
\begin{array}{l}
x+2y - (x-y) = 4-1 \\
x-y = 1
\end{array}
\right.$\\

$\Leftrightarrow
 \left\{
\begin{array}{l}
x+2y - x + y = 3 \\
x-y = 1
\end{array}
\right.$\\

$\Leftrightarrow
 \left\{
\begin{array}{l}
3y = 3 \\
x-y = 1
\end{array}
\right.$\\

$\Leftrightarrow
 \left\{
\begin{array}{l}
y = 1 \\
x-y = 1
\end{array}
\right.$\\

$\Leftrightarrow
 \left\{
\begin{array}{l}
y = 1 \\
x-1 = 1
\end{array}
\right.$\\

$\Leftrightarrow
 \left\{
\begin{array}{l}
y = 1 \\
x = 2
\end{array}
\right.$\\


\end{multicols}

On retrouve bien la même solution, le couple $(2;1)$. Pour passer du premier système au second, on a soustrait la 2eme ligne à la première.
\end{itemize}
\end{exemple*1}

\newpage

\section{Nombre de solutions d'un système}

\begin{propriete} Un système de deux équations à deux inconnues peut avoir soit aucune solution, soit un unique couple de 2 réels comme unique solution, soit une infinité de solutions. \end{propriete}

\begin{exemple*1} 
\begin{itemize}
    \item L'exemple précédent est un exemple de système possédant un unique couple solution.
    \item Le système suivant n'a pas de solutions : 
   
    $~~~~\left\{
\begin{array}{l}
x + y = 1 \\
x + y = 2
\end{array}
\right.$\\

En effet, que l'on essaye la méthode par combinaison ou substitution, on arrive à une absurdité : on souhaite en effet ici que $x+y$ vaille 1 et 2, or 1 et 2 ne sont pas égaux ! 
    \item  Le système suivant a une infinité de solutions : 
    
    $~~~~\left\{
\begin{array}{l}
x + y = 1 \\
2x + 2y = 2
\end{array}
\right.$\\

En effet, la seconde équation est similaire à la première : on a juste multiplié les deux côtés de l'équation par 2. Elle n'apporte donc pas d'informations supplémentaires sur $x$ et $y$, et il existe une infinité de couples $(x;y)$ vérifiant $x+y =1$, par exemple $(2;-1)$, $(1;0)$, $(\dfrac{1}{2};\dfrac{1}{2})$, ou bien $(0;1)$.
\end{itemize}
\end{exemple*1}

\begin{theoreme}[Règle de Cramer]
De manière générale, un système de $n$ équations indépendantes à $n$ inconnues admet une unique solution.
\end{theoreme}

\begin{remarque} On dit que des équations sont indépendantes si on peut pas obtenir une des équations en additionnant, soustrayant ou multipliant les équations précédentes ou des constantes.
\end{remarque}

\begin{exemple*1}
Dans l'exemple de système ci-dessous, les trois équations ne sont pas indépendantes : on peut obtenir la troisième en additionant la première et deux fois la deuxième.

\begin{center}
$~~~~\left\{
\begin{array}{l}
x + y -3z = 1 \\
x + 2y + z = 2 \\
3x + 5y -z = 5 
\end{array}
\right.$\\
\end{center}

Donc la règle de Cramer nous dit que ce système n'aura pas une solution unique.


\end{exemple*1}
