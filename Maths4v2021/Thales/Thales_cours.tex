
\section{Un peu d'histoire...}
\begin{historique}
En effet, même si l'on suppose son existence connue des babyloniens, la première preuve effective de la connaissance du théorème de Thalès date des égyptiens, et du papyrus de Rhind (2000 av. J.C.), document précieux dans l'étude des mathématiques en Egypte antique. Dessus, le scribe Ahmès résouds un problème irrésoluble sans le fameux théorème. 

\vspace{0.25cm}

Toutefois, comme toujours en géométrie, c'est aux Grecs auxquels on doit les premières démonstrations et en particulier à Euclide (300 av. J.C.), dans ses \textit{Eléments}.

\vspace{0.25cm}

\begin{minipage}{0.6\linewidth}
\hspace{0.5cm} L'association du nom de Thalès de Milet, mathématicien grec du 6ème siècle av. J.C. provient d'une légende autour d'un voyage en Egypte: Thalès, fasciné par les Pyramides, aurait voulu savoir la hauteur de celles-ci et s'est servi de son fameux théorème, appris auprès de scribes locaux, pour la retrouver. On lui attribue également de nombreuses réussites, comme la prédiction d'une éclipse solaire ou une bonne compréhension des crues du Nil. 
\end{minipage}
\hfill
\begin{minipage}{0.4\linewidth}
\begin{center}
\includegraphics[scale=0.8]{Thales/figures/Thales-2.eps}

Thalès de Milet
\end{center}
\end{minipage}

\vspace{0.25cm}

On notera que ce théorème porte le nom de Thalès uniquement dans les mathématiques francophones: en anglais, il est nommé \textit{intersect theorem}, le théorème d'intersection, et en allemand \textit{Strahlensatz}, c'est-à-dire théorème des rayons.
En Allemagne, on appelle "Théorème de Thalès" le fait qu'un triangle composé du diamètre d'un cercle et un autre point du cercle est rectangle.
\end{historique}


\section{Le théorème}

\begin{theoreme}[Théorème de Thalès] Soit $(BC)$ et $(DE)$ deux droites parallèles, tels que $(BD)$ et $(CE)$ soient sécantes en un point $A$. On a les égalités suivantes : 
\begin{equation*}
    \dfrac{AD}{AB} = \dfrac{AE}{AC} = \dfrac{DE}{BC}
\end{equation*}


\begin{center}
\input{Thales/figures/Cours01.tex}
\end{center}
\vspace{-0.5cm}
\end{theoreme}

\begin{remarque} La valeur de ces rapports est le coefficient de réduction ou d'aggrandissement pour passer d'un des triangles à l'autre. \end{remarque}

\begin{exemple*1} Soit ABCDE la première figure ci-dessus, avec $AD = 6$, $AB = 8$ et $DE = 9$. D'après le théorème de Thalès, on a : $\dfrac{AD}{AB} = \dfrac{AE}{AC} = \dfrac{DE}{BC}$, donc en particulier $\dfrac{AD}{AB} = \dfrac{DE}{BC} \Leftrightarrow \dfrac{6}{8} = \dfrac{9}{BC} \Leftrightarrow BC = \dfrac{8\times 9}{6} = 12$. On a donc $BC = 12$. \end{exemple*1}

\section{La réciproque}

\begin{theoreme}[Réciproque du théorème de Thalès] Soit $(DB)$ et $(CE)$ deux droites sécantes en $A$.

Si $\dfrac{AD}{AB} = \dfrac{AE}{AC}$, et si les points $A$, $B$ et $D$ sont nés dans le même ordre que $A$, $C$ et $E$, alors les droites $(BC)$ et $(DE)$ sont parallèles.
\end{theoreme}

\begin{remarques}

\begin{itemize}
    \item On se servira de ceci pour démontrer le parallèlisme de deux droites.
    \item On a besoin que de deux des trois rapports pour la réciproque.
\end{itemize}
\end{remarques}

\begin{exemple*1} En reprenant de nouveau la première figure ci-dessus, cette fois avec $ AD = 3$, $AB=6$, $AE = 5$ et $AC=10$.

On constate que $\dfrac{AD}{AB} = \dfrac{3}{6} = \dfrac{1}{2}$ et $\dfrac{5}{10} = \dfrac{1}{2}$. Et on a de plus l'aligneéent des points. En conséquence, d'après la réciproque du théorème de Thalès les droites sont parallèles.
\end{exemple*1}

\section{La contraposée}

\begin{theoreme}[Contraposée du théorème de Thalès]  Soit $(DB)$ et $(CE)$ deux droites sécantes en $A$.

Si $\dfrac{AD}{AB} \ne \dfrac{AE}{AC}$, et si les points $A$, $B$ et $D$ sont nés dans le même ordre que $A$, $C$ et $E$, alors les droites $(BC)$ et $(DE)$ ne sont pas parallèles.
\end{theoreme}

\begin{exemple*1} En reprenant de nouveau la première figure ci-dessus, cette fois avec $ AD = 3$, $AB=6$, $AE = 5$ et $AC=7$.

On constate que $\dfrac{AD}{AB} = \dfrac{3}{6} = \dfrac{1}{2}$ et $\dfrac{5}{7} \ne \dfrac{1}{2}$. Et on a de plus l'alignement des points. En conséquence, d'après la contraposée du théorème de Thalès les droites ne sont pas parallèles.
\end{exemple*1}

\section{Agrandissements et réductions}

\begin{definition}
Lorsque deux figures ont la même forme et des longueurs proportionnelles, on dit que l'une est un agrandissement ou une réduction de l'autre.
\end{definition}

\hspace{-0.6cm}
\begin{minipage}{0.6\linewidth}
\begin{remarque}
Le coefficient de proportionnalité $k$ des longueurs détermine s'il s'agit d'un agrandissement ou d'une réduction : 
\begin{itemize}
    \item Si $0<k<1$, alors on parle de réduction. $k$ est alors le rapport de réduction.
    \item Si $k>1$, alors on parle d'agrandissement. $k$ est alors le rapport d'agrandissement.
\end{itemize}
\end{remarque}
\end{minipage}
\hfill
\begin{minipage}{0.4\linewidth}

\begin{center}
\hspace{-5cm}
\input{Thales/figures/Cours02.tex}
\end{center}
\end{minipage}


\begin{proprietes}
\begin{itemize}
    \item Dans un agrandissement ou une réduction, les mesures des angles, la perpendicularité et le parallélisme sont conservés.
    \item Dans un agrandissement ou une réduction de rapport $k$, les longueurs sont multipliées par $k$, les aires par $k^2$, et les volumes par $k^3$.
\end{itemize}
\end{proprietes}

\begin{exemple*1}
Si on agrandit une pyramide avec un facteur d'agrandissement de 3, la hauteur de la pyramide est multipliée par 3, l'aire de sa base par 9 et son volume par 27.
\end{exemple*1}


\vfill

\begin{autoeval}
\begin{enumerate}
    \item Si on a $\dfrac{3}{x} = \dfrac{5}{2}$, que vaut $x$?
        \begin{center}
        \begin{multicols}{4}
            \begin{enumerate}
                \item $\dfrac{10}{3}$
                \columnbreak
                \item $\dfrac{5}{6}$
                \columnbreak
                \item  $\dfrac{15}{2}$
                \columnbreak
                \item Une autre réponse.
                \columnbreak
            \end{enumerate}
        \end{multicols}
    \end{center}
    

    \begin{minipage}{0.7\linewidth}    
    \item Que vaut $AB$ ? On suppose que $()$//$()$.
    
    \textit{La figure n'est pas à l'échelle.}

    \begin{center}
        \begin{multicols}{2}
            \begin{enumerate}
                \item 2
                \item 3
                \item 4
                \item Une autre réponse
            \end{enumerate}
        \end{multicols}
    \end{center}
    \end{minipage}
    \hfill
    \begin{minipage}{0.3\linewidth}
    \begin{center}
        \begin{adjustbox}{width=1\linewidth}
            \input{Thales/figures/AutoEval01}        
        \end{adjustbox}
    \end{center}
    \end{minipage}
    
    \begin{minipage}{0.7\linewidth} 
    \item Le petit triangle est-il une réduction du grand ? 
    
    \textit{La figure n'est pas à l'échelle.}
    \begin{center}
        \begin{multicols}{2}
            \begin{enumerate}
                \item Oui
                \columnbreak
                \item Non
            \end{enumerate}
        \end{multicols}
    \end{center}
    \end{minipage}
    \hfill
    \begin{minipage}{0.3\linewidth}
    \begin{center}
        \begin{adjustbox}{width=1\linewidth}
            \input{Thales/figures/AutoEval02}        
        \end{adjustbox}
    \end{center}
    \end{minipage}
\end{enumerate}


\begin{flushright}
\rotatebox{180}{Si tu as 3 réponses, choisis le chemin rouge. Si tu en as 2, le chemin jaune. Sinon, choisis le chemin vert.}
\rotatebox{180}{\textbf{Les solutions sont} : d-c-b.}
\end{flushright}
\vspace{-1cm}

\end{autoeval}